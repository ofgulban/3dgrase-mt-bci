# -*- coding: utf-8 -*-
"""
3D-GRASE MT project .pickle reader & custom .txt creator

Uses psychopy.misc because pickle.load function gives an error.
Will be used to create required custom .txt files by various software.

@author: Omer Faruk Gulban
"""

from psychopy import misc
from os import mkdir
from pprint import pprint

#%%
"""Please enter the exact name of the pickle file"""

inputFileName = 'Test-MAIN_ATTENTION_RUN-00-Mar_20_2014_20_05.pickle'


#%%
try:
    pickle = misc.fromFile(inputFileName)
except:
    print '!!!\nFile cannot be read!\n!!!'

#%% some easy to use notes

pprint(pickle)

print '---'

print pickle['Blocks']

print '---'

for i, j in enumerate(pickle['Blocks'][0]):
    if j in range(0,4):
        print pickle['Block_Durations'][0][i]

print '---'

#%% definitions
offsetIdx  = []
staticIdx  = []
horiIdx = []
vertIdx = []
targetIdx  = []

#%% file operations

filename = pickle['SubjectID'] +'-'+ pickle['ScriptID'] +'-'+ pickle['Run_Number'] +'-'+ pickle['Date'] + '.txt'
filePath = 'PROTOCOLS'

try:
    mkdir(filePath, 0777)
except:
    print '(OUTPUT folder has already been created.)'

file = open(filePath +'/'+ filename, 'w')

strings = ['FileVersion: 2\n',
           'ResolutionOfTime: Volumes\n',
           'Experiment: untitled\n',
           'BackgroundColor: 0 0 0\n',
           'TextColor: 255 255 217\n',
           'TimeCourseColor: 255 255 255\n',
           'TimeCourseThick: 3\n',
           'ReferenceFuncColor: 255 255 51\n',
           'ReferenceFuncThick: 2\n',
           'NrOfConditions: 4\n'
           ]

file.writelines(strings)

# find the indices of the conditions
for i, j in enumerate(pickle['Blocks'][0]):
    
    if j == -1:
        offsetIdx.append(i)
        
    if j in range(0,4):
        staticIdx.append(i)
    
    # change ranges according to script ID (main run or single component run)
    if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
        if j in range(4,12):
            vertIdx.append(i)        
    
    if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
        if j in range(4,20):
            vertIdx.append(i)    

    if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
        if j in range(12,20):
            horiIdx.append(i)        
    
    if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
        if j in range(20,36):
            horiIdx.append(i)
            
    if j in range(96,100):
        targetIdx.append(i)

#%%----------------------------------------------------------------------------
"""OFFSET"""

file.writelines(['\n','OFFSET\n','2'])
        
for i, j in enumerate(offsetIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 255 255 255')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""STATIC"""

file.writelines(['\n','STATIC\n','8'])
   
for i, j in enumerate(staticIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 200 200 200')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""ATTENTION_VERTICAL_MOTION"""

if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
    file.writelines(['\n','VERT_MO\n','16'])
elif pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
    file.writelines(['\n','ATTVERT\n','16'])

for i, j in enumerate(vertIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 255 0 0')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""ATTENTION_HORIZONTAL_MOTION"""

if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
    file.writelines(['\n','HORI_MO\n','16'])
elif pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
    file.writelines(['\n','ATTHORI\n','16'])

for i, j in enumerate(horiIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 0 255 0')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""TARGET_DETECTION"""

if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
	file.writelines(['\n','TARGET\n','8'])

	for i, j in enumerate(targetIdx):
		file.write('\n')
		file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
		file.write(' ')
		file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

	# contiditon color
	file.write('\nColor: 0 0 255')


file.close()