# -*- coding: utf-8 -*-
"""
3D-GRASE MT project .pickle reader & custom .txt creator

Uses psychopy.misc because pickle.load function gives an error.
Will be used to create required custom .txt files by various software.

@author: Omer Faruk Gulban
"""

from psychopy import misc
from os import mkdir
from pprint import pprint

#%%
"""Please enter the exact name of the pickle file"""

inputFileName = 'EXAMPLE.pickle'

#%%
try:
    pickle = misc.fromFile(inputFileName)
except:
    print '!!!\nFile cannot be read!\n!!!'

#assign run number to a short variable
runNr = str(int(pickle['Run_Number'])-1) #pymvpa wants runs to start from '0'

#%% some easy to use notes

pprint(pickle)

print '---'

print pickle['Blocks']

print '---'

for i, j in enumerate(pickle['Blocks'][0]):
    if j in range(0,4):
        print pickle['Block_Durations'][0][i]

print '---'

#%% definitions
offsetIdx  = []
staticIdx  = []
horiIdx = []
vertIdx = []
targetIdx  = []

#%% file operations

filename = pickle['SubjectID'] +'-'+ pickle['ScriptID'] +'-'+ pickle['Run_Number'] +'-'+ pickle['Date'] + '.txt'
filePath = 'PROTOCOLS\PyMVPA'

# check and create output fodler
try:
    mkdir(filePath, 0777)
except:
    print '(OUTPUT folder has already been created.)'

file = open(filePath +'/'+ filename, 'w')

for i, j in enumerate(pickle['Blocks'][0]):

    for k in range(0,int(pickle['Block_Durations'][0][i])):

        # offset
        if j == -1:
            file.writelines(['OFFSET\t'+runNr+'\n'])
        
        # static
        if j in range(0,4):
            file.writelines(['STATIC\t'+runNr+'\n'])

        if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
            # vertical
            if j in range(4,12):
                file.writelines(['VERT\t'+runNr+'\n'])
            # horizontal
            if j in range(12,20):
                file.writelines(['HORI\t'+runNr+'\n'])

        if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
            # vertical
            if j in range(4,20):
                file.writelines(['ATTVERT\t'+runNr+'\n'])
            # horizontal
            if j in range(20,36):
                file.writelines(['ATTHORI\t'+runNr+'\n'])
        
        # target
        if j in range(96,100):
            file.writelines(['TARGET\t'+runNr+'\n'])

file.close()