# -*- coding: utf-8 -*-
"""
3D-GRASE MT project main(attention) run script.

Uses Psychopy.

Red fixation dot is to attend horizontal motion.
Green fixation dot is to attend vertical motion.
Hit ESC or q to quit.

Nr. of Measurements: 320

@author: Omer Faruk Gulban
"""

import numpy as np
from copy import copy
from psychopy import visual, core, event, filters, monitors, parallel, misc
from os import mkdir
import time #to get date for the output file naming

#%%
"""GENERAL_OPTIONS"""

subjectID = 'Test'
runNumber = '00'

fullScreen  = True
stimSize    = 12
gratC       = (0.06,-0.06) #grating colors (original demo value was 0.4)
speedValue  = 0.055
horiColor   = [ 1,-1, -1]
vertColor   = [-1, 1, -1]

useParallelPort   = False
parallelPin       = 10

triggerKey        = '5'
responseButtonKey = '1'

scriptID  = 'MAIN_ATTENTION_RUN'
projectID = '3DGRASE_MT'

date = time.strftime("%b_%d_%Y_%H_%M", time.localtime())

#%%
"""MONITOR"""

# set monitor information used in the experimental setup
moni = monitors.Monitor('testMonitor', width=30, distance=99) #cm,

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = fullScreen ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
mywin.setMouseVisible(False)
#%%
"""FIXATION"""

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     =-0.2, 
                              size      = 1, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'black' , 
                            size    = 0.13  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

#%%
"""RAISEDCOS_MASK"""

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          contrast=0, size=stimSize)
mask.setAutoDraw(True)

#%%
"""PERIPHERAL_MASK"""

#easy to use switch
peripheryMaskPowerSwitch=True

#TODO:Working but bad solution for peripheral masking
backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(stimSize,15), color='gray')
backgroundMask.setPos((-stimSize,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(stimSize,15), color='gray')
backgroundMask.setPos((+stimSize,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(15,stimSize), color='gray')
backgroundMask.setPos((0,-stimSize))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(15,stimSize), color='gray')
backgroundMask.setPos((0,+stimSize))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

#%%
"""TIME_TEST_TEXT"""

testText = visual.TextStim(win=mywin, font="sans", height=0.3, pos=(0,-2.5), units='deg')
testText.setColor('Black')
testText.setAutoDraw(False)

#%%
"""PIECEWISE_GRATINGS"""

gratMask2 = -np.ones((16,16))
gratMask2[3::4,2::4] = 1
gratMask2[3::4,1::4] = 1

redPatch1o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg',
                                color=gratC[0],
                                size=stimSize,
                                sf=1,
                                ori=90,
                                interpolate=False,
                                )

redPatch2o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg', 
                                color=gratC[1],
                                size=stimSize,
                                sf=1,
                                ori=0,
                                interpolate=False,
                                )

#%%
"""NOISE_CREATION"""

#params
noiseColor = 0 #color of the noise

#noise related parameters
noiseReso = 128              #resolution of the noise
pixInGrating = noiseReso/4   #number of pixels within the width of a grating bar
noiseDensityFactor = 64 #higher values means less noise
graRatioNum = 3     #numerator (num&denum seperated due to type difference errors)
graRatioDenum = 4   #denumerator

#noise matrix operations
noiseMatrix = np.ones((noiseReso, noiseReso))
noiseMatrix[:,0::noiseDensityFactor] = -1

map(np.random.shuffle, noiseMatrix)
nMask = -noiseMatrix

#for row masking to use little later
nMaskRow = copy(nMask)

#noise column mask[for loop manipulates individual columns with jumps per iteration]
namelessParam = pixInGrating*graRatioNum/graRatioDenum
for i in range(0,namelessParam):
    nMask[:,i::pixInGrating] = -1

#cant think of an easy way now, just to delete non-overlaping noise (with piecewise gratings)
for i in range(0,namelessParam/3):
    nMask[i::pixInGrating] = -1
for i in range(0,namelessParam/3):
    nMask[i+24::pixInGrating] = -1

#noise row mask
for i in range(0,namelessParam/3):
    nMaskRow[i::pixInGrating] = -1
#ugly solution as above (TODO:think of a better way)
for i in range(0,namelessParam/3):
    nMaskRow[i+8::pixInGrating] = -1
for i in range(0,namelessParam/3):
    nMaskRow[i+16::pixInGrating] = -1    
for i in range(0,namelessParam/3):
    nMaskRow[:,i::pixInGrating] = -1
for i in range(0,namelessParam/3):
    nMaskRow[:,i+24::pixInGrating] = -1

blackPatch1 = visual.GratingStim(win=mywin, tex=None, mask=nMask, units='deg',
                                size=stimSize,
                                color=noiseColor,
                                interpolate=False,
                                )

blackPatch2 = visual.GratingStim(win=mywin, tex=None, mask=nMaskRow, units='deg',
                                size=stimSize,
                                color=noiseColor,
                                interpolate=False,
                                )

#%%
"""SET_POSITION_PREPERATION"""

pos1 = ( 0, 0)
pos2 = (-stimSize, 0)
pos3 = ( 0, 0)
pos4 = ( 0,-stimSize) 

#%%
"""BLOCKS"""

baseBlockDur = 6 #measurements (not in seconds!)

# different numbers are blocks, same numbers indicate repetitions
blocks=np.zeros((1,36)) #static
blocks[0,1 ] = 1        #static
blocks[0,2 ] = 2        #static
blocks[0,3 ] = 3        #static
blocks[0,4 ] = 4        #attend vertical
blocks[0,5 ] = 5        #attend vertical
blocks[0,6 ] = 6        #attend vertical
blocks[0,7 ] = 7        #attend vertical
blocks[0,8 ] = 8        #attend vertical
blocks[0,9 ] = 9        #attend vertical
blocks[0,10] = 10       #attend vertical
blocks[0,11] = 11       #attend vertical
blocks[0,12] = 12       #attend vertical
blocks[0,13] = 13       #attend vertical
blocks[0,14] = 14       #attend vertical
blocks[0,15] = 15       #attend vertical
blocks[0,16] = 16       #attend vertical
blocks[0,17] = 17       #attend vertical
blocks[0,18] = 18       #attend vertical
blocks[0,19] = 19       #attend vertical
blocks[0,20] = 20       #attend horizontal
blocks[0,21] = 21       #attend horizontal
blocks[0,22] = 22       #attend horizontal
blocks[0,23] = 23       #attend horizontal
blocks[0,24] = 24       #attend horizontal
blocks[0,25] = 25       #attend horizontal
blocks[0,26] = 26       #attend horizontal
blocks[0,27] = 27       #attend horizontal
blocks[0,28] = 28       #attend horizontal
blocks[0,29] = 29       #attend horizontal
blocks[0,30] = 30       #attend horizontal
blocks[0,31] = 31       #attend horizontal
blocks[0,32] = 32       #attend horizontal
blocks[0,33] = 33       #attend horizontal
blocks[0,34] = 34       #attend horizontal
blocks[0,35] = 35       #attend horizontal

#extra static conditions
blocks = np.append(blocks, [[0,1,2,3]], axis=1)

#target detection conditions
blocks = np.append(blocks, [[99,99,98,98,97,97,96,96]], axis=1)

# shuffle
map(np.random.shuffle, blocks)

#%%
"""JITTER"""

jitter=np.zeros((1,48))
jitter[0, 0:24]  = 1 #1 measurement as positive jitter (not in seconds!)

# shuffle
map(np.random.shuffle, jitter)

#block durations after shuffling
blockDur = np.zeros((1,48))
blockDur = jitter + baseBlockDur

#%%
"""OFFSET"""

blocks   = np.insert(blocks  , 0, -1, axis=1)     #insert offset condition
blockDur = np.insert(blockDur, 0,  5, axis=1)     #5 measurements of initial offset
blocks   = np.append(blocks  , [[-1]], axis=1)    #insert final offset condition
blockDur = np.append(blockDur, [[ 3]], axis=1)    #3 measurements final offset

print "Blocks_array:"; print blocks; print "---"
print "Block_duration_array:";print blockDur; print "---"

#%%
"""TIME"""

# params
fMRI_TR = 2
totalTime = np.sum(blockDur) * fMRI_TR

# just for cosmetic reasons 
totalTrigs = np.sum(blockDur)

print "Total_duration: %.3f min or %i sec" %(totalTime/60, totalTime)
print "Number of measurements: %i" %(np.sum(blockDur))
print "---"

# give the system time to settle
core.wait(0.5)

# create a clock
globalClock=core.Clock()
globalClock.reset()

#%%
"""SWITCHES"""

# trigger related
trigCounter = 0

# stimulus switches (important to change drawing order)
targetSwitch1 = -1
targetSwitch2 = -1

layerSwitch = 1# used to change the drawing order

initialOffset = True
targetAppeared = -1
responseSwitch = -1
targetDetectCount = 0

#%%
"""RENDER_LOOP"""

scannerStartTrigger = True
while scannerStartTrigger:

    if useParallelPort and parallel.readPin(parallelPin) == 1:
        scannerStartTrigger = False

    for keys in event.getKeys():        

        if keys in [triggerKey]:
            scannerStartTrigger = False

        elif keys[0]in ['escape','q']:
            mywin.close()
            core.quit()

i=0
interruption=False

while trigCounter<totalTrigs:

    #print which condition and index            
    print "Block_index___: %i [condition: %i]" %(i, blocks[0,i])

    if blocks[0,i] == -1: #offset
        fixDot.setColor('Black')
        posStepHori = 0
        posStepVert = 0
        targetSwitch1 = -1
        targetSwitch2 = -1
        
        if initialOffset:        
            #randomized layer swap
            layerSwitch = np.random.randint(2)*2-1
            #randomized color swap
            offsetRandomNr1 = np.random.randint(2)
            redPatch1o.setColor(gratC[offsetRandomNr1])
            redPatch2o.setColor(gratC[offsetRandomNr1-1])
            #randomized offset variations
            offsetRandomNr2 = np.random.random()
            pos1 = (offsetRandomNr2, 0)
            pos2 = (offsetRandomNr2-stimSize, 0)
            pos3 = ( 0, offsetRandomNr2)
            pos4 = ( 0, offsetRandomNr2-stimSize) 
            initialOffset=False

    elif blocks[0,i] == 0: #static
        fixDot.setColor('Black')
        posStepHori=0
        posStepVert=0
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        
    elif blocks[0,i] == 1: #static
        fixDot.setColor('Black')
        posStepHori=0
        posStepVert=0
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
    
    elif blocks[0,i] == 2: #static
        fixDot.setColor('Black')
        posStepHori=0
        posStepVert=0
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])

    elif blocks[0,i] == 3: #static
        fixDot.setColor('Black')
        posStepHori=0
        posStepVert=0
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        
#Vertical_attention_conditions------------------------------------------------- 

    elif blocks[0,i] == 4: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])       
        posStepHori = speedValue
        posStepVert = speedValue
        
    elif blocks[0,i] == 5: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 6: #vert    
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 7: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 8: #vert COLOR SWITCH
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 9: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = speedValue
        
    elif blocks[0,i] == 10: #vert   
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 11: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 12: #vert LAYERS SWITCH
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 13: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 14: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 15: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 16: #vert COLOR SWITCH
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 17: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 18: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = -speedValue


    elif blocks[0,i] == 19: #vert
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = -speedValue

#Horizontal_attention_conditions-----------------------------------------------

    elif blocks[0,i] == 20: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])       
        posStepHori = speedValue
        posStepVert = speedValue
        
    elif blocks[0,i] == 21: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 22: #hori    
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = -speedValue


    elif blocks[0,i] == 23: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 24: #hori COLOR SWITCH
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 25: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = speedValue
        
    elif blocks[0,i] == 26: #hori   
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 27: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 28: #hori LAYERS SWITCH
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 29: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 30: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 31: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 32: #hori COLOR SWITCH
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 33: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = speedValue

    elif blocks[0,i] == 34: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = -speedValue

    elif blocks[0,i] == 35: #hori
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = -1
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = -speedValue

#Target_Detection--------------------------------------------------------------

    elif blocks[0,i] == 99: #???
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = 1
        targetSwitch2 = -1
        layerSwitch   = (np.random.randint(0,2)*2-1)
        responseSwitch = 1

        #randomized color swap
        attRandomNr1 = np.random.randint(2)
        redPatch1o.setColor(gratC[attRandomNr1])
        redPatch2o.setColor(gratC[attRandomNr1-1])

        #random motion direction
        posStepHori = (np.random.randint(0,2)*2-1)*speedValue
        posStepVert = (np.random.randint(0,2)*2-1)*speedValue

    elif blocks[0,i] == 98: #???
        fixDot.setColor(vertColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = 1
        layerSwitch   = (np.random.randint(0,2)*2-1)
        responseSwitch = 1

        #randomized color swap
        attRandomNr1 = np.random.randint(2)
        redPatch1o.setColor(gratC[attRandomNr1])
        redPatch2o.setColor(gratC[attRandomNr1-1])

        #random motion direction
        posStepHori = (np.random.randint(0,2)*2-1)*speedValue
        posStepVert = (np.random.randint(0,2)*2-1)*speedValue

    elif blocks[0,i] == 97: #???
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = 1
        targetSwitch2 = -1
        layerSwitch   = (np.random.randint(0,2)*2-1)
        responseSwitch = 1

        #randomized color swap
        attRandomNr1 = np.random.randint(2)
        redPatch1o.setColor(gratC[attRandomNr1])
        redPatch2o.setColor(gratC[attRandomNr1-1])

        #random motion direction
        posStepHori = (np.random.randint(0,2)*2-1)*speedValue
        posStepVert = (np.random.randint(0,2)*2-1)*speedValue

    elif blocks[0,i] == 96: #???
        fixDot.setColor(horiColor, 'rgb')
        targetSwitch1 = -1
        targetSwitch2 = 1
        layerSwitch   = (np.random.randint(0,2)*2-1)
        responseSwitch = 1

        #randomized color swap
        attRandomNr1 = np.random.randint(2)
        redPatch1o.setColor(gratC[attRandomNr1])
        redPatch2o.setColor(gratC[attRandomNr1-1])

        #random motion direction
        posStepHori = (np.random.randint(0,2)*2-1)*speedValue
        posStepVert = (np.random.randint(0,2)*2-1)*speedValue

#------------------------------------------------------------------------------
    
    while trigCounter<np.sum(blockDur[0,0:i+1]):

        #set positions for motion
        pos1 = ((pos1[0]+posStepVert)%stimSize  ,pos1[1])
        pos2 = ((pos2[0]+posStepVert)%-stimSize ,pos2[1])
        pos3 = (pos3[0], (pos3[1]+posStepHori)%stimSize )
        pos4 = (pos4[0], (pos4[1]+posStepHori)%-stimSize)

        #----------------------------------------------------------------------

        if layerSwitch ==1:
            redPatch1o.setPos(pos1)
            redPatch1o.draw()
            redPatch1o.setPos(pos2)
            redPatch1o.draw()
        
            #part after 'and' is for jittering the appearance of target
            if targetSwitch1 == 1 and (trigCounter-np.sum(blockDur[0,0:i+1]))>np.random.randint(-6,-3):
                blackPatch1.setPos(pos1)
                blackPatch1.draw()
                blackPatch1.setPos(pos2)    
                blackPatch1.draw()
                targetAppeared = 1
                
        #----------------------------------------------------------------------
    
        redPatch2o.setPos(pos3)
        redPatch2o.draw()
        redPatch2o.setPos(pos4)
        redPatch2o.draw()
    
        if targetSwitch2 == 1 and (trigCounter-np.sum(blockDur[0,0:i+1]))>np.random.randint(-6,-3):
            blackPatch2.setPos(pos3)
            blackPatch2.draw()
            blackPatch2.setPos(pos4)    
            blackPatch2.draw()
            targetAppeared = 1
            
        #----------------------------------------------------------------------
    
        if layerSwitch ==-1:
            redPatch1o.setPos(pos1)
            redPatch1o.draw()
            redPatch1o.setPos(pos2)
            redPatch1o.draw()
        
            #part after 'and' is for jittering the appearance of target
            if targetSwitch1 == 1 and (trigCounter-np.sum(blockDur[0,0:i+1]))>np.random.randint(-6,-3):
                blackPatch1.setPos(pos1)
                blackPatch1.draw()
                blackPatch1.setPos(pos2)    
                blackPatch1.draw()
                targetAppeared = 1
    
        #----------------------------------------------------------------------

#        #globalClock on screen print (Beware: updating the text results FPS drops)
#        testText.setText("Block countdown = %.i" %(trigCounter-np.sum(blockDur[0,0:i+1])))
  
        mywin.flip()

        #parallel port trigger counter (see key press section below for usb triggers)
        if useParallelPort and parallel.readPin(parallelPin) == 1:
            trigCounter = trigCounter + 1

        #handle key presses each frame
        for keys in event.getKeys(timeStamped=True):
            if keys[0]in ['escape','q']:
                interruption = True

            if keys[0]in [triggerKey]:
                trigCounter = trigCounter + 1
                
            if keys[0]in [responseButtonKey]:
               if targetAppeared==1 and responseSwitch==1 and blocks[0,i] > 90: #90 because its the range I used for target conditions
                   targetDetectCount = targetDetectCount + 1
                   print "Response!"
                   responseSwitch = -1 #turn off response listening
                   
   #break out nested loops
        if interruption:break
    if interruption:
        print '!!! \nInterrupted! \n!!! '
        break
                   
    #print global time and condition duration to check time slips
    print "Global_Trigger_Count: %.i" %trigCounter
    print "Block_duration: %.i" %(trigCounter-np.sum(blockDur[0,0:i]))
    print "---"
        
    #iterate i
    i=i+1

    #targetSwitch quickfix (patch, not in line with the general logic of the script)
    targetSwitch = -1
    responseSwitch = -1
    
# timing accuracy prints
finalTime = globalClock.getTime()

print "Number of correct detections: %.2f%%" %((targetDetectCount/8.)*100)
print '---'
print "Finished in: %i seconds" %finalTime
print "***"

#%%
"""OUTPUT"""

output = {'ProjectID'       : projectID,
          'ScriptID'        : scriptID,
          'Date'            : date,
          'SubjectID'       : subjectID,
          'Run_Number'      : runNumber,
          'Blocks'          : blocks,
          'Block_Durations' : blockDur,
          'Trigger_Count'   : trigCounter,
          'Final_Time'      : finalTime,
          'Interruption'    : interruption,
          'Stimulus_Size'   : stimSize,
          'Grating_Color'   : gratC,
          'Speed_Value'     : speedValue,
          'Noise_Color'     : noiseColor,
          'Noise_Resolution': noiseReso,
          'Target_Detection_Count': targetDetectCount,
          }
try:
    mkdir('OUTPUT', 0777)
except:
    print '(OUTPUT folder has already been created.)'

outFileName = subjectID +'-'+ scriptID +'-'+ runNumber +'-'+ date
misc.toFile('OUTPUT/'+ outFileName +'.pickle', output)
print 'Log Data saved as: '+ outFileName +'.pickle'
print "***" 

#%% cleanup
mywin.close()
core.quit()