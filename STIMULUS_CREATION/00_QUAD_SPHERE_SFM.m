% main chunk of the code is from 'http://peterscarfe.com/ptbtutorials.html'
% structure from motion demo

% Clear the workspace
clear all; close all; sca;

% Here we call some default settings for setting up Psychtoolbox
PsychDefaultSetup(2);

% Skip sync tests for this demo in case people are using a defective
% system. This is for demo purposes only.
Screen('Preference', 'SkipSyncTests', 2);

%--------------------------------------------------------------------------
%                       Screen initialisation
%--------------------------------------------------------------------------

% Find the screen to use for displaying the stimuli. By using "max" this
% will display on an external monitor if onqe is connected.
screenid = max(Screen('Screens'));

% Determine the values of black and white
black = BlackIndex(screenid);
white = WhiteIndex(screenid);
gray = WhiteIndex(screenid)/1;

% Set up our screen
[window, windowRect] = PsychImaging('OpenWindow', screenid, black, [], 32, 2);

% Get the width and height of the window in pixels
[screenXpix, screenYpix] = Screen('WindowSize', window);

% Determine the center of the screen. We will need this later when we draw
% our dots.
[center(1), center(2)] = RectCenter(windowRect);

% We assume some screen dimensions here so that the stimulus will fit
% nicely on the screen
screenYcm = 30;
screenXcm = 30 * (screenXpix / screenYpix);
cmPerPix = screenXcm / screenXpix;
pixPerCm = screenXpix / screenXcm;

% Set the blend function so that we get nice antialised edges to the dots
% defining our cyliner
Screen('BlendFunction', window, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

%--------------------------------------------------------------------------
%                   Stimulus information
%--------------------------------------------------------------------------

% Cylinder height, width and radius

cylWidth  = 10;
cylHeight = cylWidth;
cylRadius = cylWidth / 2;

% Numer of dots to place over the surface of the cylinder
numDots = 500;

% Dot track centres over the height of the cylinder
ypos  = (rand(1, numDots) .* 2 - 1) .* cylHeight .* pixPerCm;
ypos2 = (rand(1, numDots) .* 2 - 1) .* cylHeight .* pixPerCm;

% Randomly assign the dots angles. This determines their X position on the
% screen. We are using ortographic projection, so the dots do not have a Z
% position.
angles = rand(1, numDots) .* 360;

% Set the dot size in pixels
dotSizePixels = 3;

% Set fixation patch circle size and position
fixPatchSize = 50;
fixPatchPos  = [center(1)-fixPatchSize center(2)-fixPatchSize ...
               center(1)+fixPatchSize center(2)+fixPatchSize];

% Set fixation dot size and position
fixDotSize = 4;
fixDotPos  = [center(1)-fixDotSize center(2)-fixDotSize ...
              center(1)+fixDotSize center(2)+fixDotSize];

          
%--------------------------------------------------------------------------
%                       Center-Surround Idea
%--------------------------------------------------------------------------          

% Set big noise parameters

bigWidth  = 12;
bigHeight = bigWidth;
bigRadius = bigWidth / 2;

ypos3 = (rand(1, numDots) .* 2 - 1) .* bigHeight .* pixPerCm;

% Set big black patch behind
bigBlackSize = 15;
bigBlackPos  = [center(1)-bigBlackSize center(2)-bigBlackSize ...
                center(1)+bigBlackSize center(2)+bigBlackSize];          
          
          
%--------------------------------------------------------------------------
%                           Drawing Loop
%--------------------------------------------------------------------------

tic;  %for fraps

% Stimulus drawing loop (exits when any button is pressed)
while ~KbCheck

    % Calculate the X screen position of the dots (note we have to convert
    % from degrees to radians here.
    
    xpos = cos(angles .* (pi / 180)) * cylWidth.* pixPerCm; 
    xmult = (1-((ypos/(cylHeight * pixPerCm)).^2)).^0.5; % x^2+y^2=1
    xpos = xpos.*xmult;
    
    xpos2 = cos(angles .* (pi / 180)) * cylWidth.* pixPerCm; 
    xmult2 = (1-((ypos2/(cylHeight * pixPerCm)).^2)).^0.5; 
    xpos2 = xpos2.*xmult2;
    
    %%%%%%% Big noise related%%%%%%%%%%%%%%
    xpos3 = cos(angles .* (pi / 180)) * bigWidth.* pixPerCm; 
    xmult3 = (1-((ypos3/(bigHeight * pixPerCm)).^2)).^0.5; 
    xpos3 = xpos3.*xmult3;
    
    Screen('DrawDots', window, [xpos3; ypos3], dotSizePixels, gray, center, 2);
    Screen('FillOval', window, [-1 -1 -1], bigBlackPos);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    % Draw the dots. Here we set them to white, determine the point at
    % which the dots are drawn relative to, in this case our screen center.
    % And set anti-aliasing to 1. This gives use smooth dots. If you use 0
    % instead you will get squares. And if you use 2 you will get nicer
    % anti-aliasing of the dots.
    Screen('DrawDots', window, [xpos; ypos], dotSizePixels, gray, center, 2);
    Screen('DrawDots', window, [ypos2; xpos2], dotSizePixels, gray, center, 2);

    % Fixation dot & patch
    Screen('FillOval', window, [-1 -1 -1], fixPatchPos);
    Screen('FillOval', window, [ 1 -1 -1], fixDotPos);
    
    % Flip to the screen
    Screen('Flip', window);

    % Increment the angle of the dots by one degree per frame
    angles = angles + 0.60;

end

% Clean up and leave the building
sca; clear all;