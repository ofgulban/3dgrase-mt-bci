# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 11:13:54 2014

Hit ESC to quit

Press '3' or '4' for luminance adjustments.
Press '5' or '6' for speed adjustments.
Press 'r' to reset.
Press 't' or 'y' for targets (noise on piecewise gratings)
Press 'o' to change motion directions (only two for now)

@author: Omer Faruk Gulban
"""

import numpy as np
from copy import copy
from psychopy import visual, core, event, filters, monitors

#%%
# set monitor information used in the experimental setup
moni = monitors.Monitor('UMRAM', width=8.2, distance=60) #cm,
"""
According to 'Silent Vision Model SV-7021' documentation.
"""

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )

#%%
"""FIXATION"""

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     =-0.2, 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'black' , 
                            size    = 0.05  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

#%%
"""RAISEDCOS_MASK"""

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          contrast=0, size=4)
mask.setAutoDraw(True)

#%%
"""PERIPHERAL_MASK"""

#easy to use switch
peripheryMaskPowerSwitch=True

#TODO:Working but bad solution for peripheral masking
backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(4,7), color='gray')
backgroundMask.setPos((-4,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg',size=(4,7), color='gray')
backgroundMask.setPos((+4,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg',size=(7,4), color='gray')
backgroundMask.setPos((0,-4))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg',size=(7,4), color='gray')
backgroundMask.setPos((0,+4))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

#%%
"""GRATINGS"""

#params
grat_1 = 0.3
grat_2 =-0.3
solid1 = 1
backgr =-1

# use same mask change orientation
gratMask = -np.ones((16,16))
gratMask[3::4] = 1 #starts from 3 to match with the stimulus

redPatch1 = visual.GratingStim(win=mywin, tex=None, mask=gratMask, units='deg',
                               color=grat_1,
                               size=4,
                               sf=1,
                               ori=90,
                               interpolate=False,
                               )

redPatch2 = visual.GratingStim(win=mywin, tex=None, mask=gratMask, units='deg', 
                               color=grat_2,
                               size=4,
                               sf=4,
                               ori=0,
                               interpolate=False,
                               )
#%%
"""COLOR_VARIATION_OVERLAYS"""

#params
colorDevience = 0.1

gratMask2 = -np.ones((16,16))
gratMask2[3::4,2::4] = 1
gratMask2[3::4,1::4] = 1

redPatch1o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg',
                                color=grat_1+colorDevience,
                                size=4,
                                sf=1,
                                ori=90,
                                interpolate=False,
                                )

redPatch2o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg', 
                                color=grat_2-colorDevience,
                                size=4,
                                sf=4,
                                ori=0,
                                interpolate=False,
                                )

#%%
"""NOISE_CREATION"""

#params
lumColor = 0 #color of the noise

#noise related parameters
reso = 256              #resolution of the noise
pixInGrating = reso/4   #number of pixels within the width of a grating bar
noiseDensityFactor = 64 #higher values means less noise
graRatioNum = 3     #numerator (num&denum seperated due to type difference errors)
graRatioDenum = 4   #denumerator

#noise matrix operations
noiseMatrix = np.ones((reso,reso))
noiseMatrix[:,0::noiseDensityFactor] = -1

map(np.random.shuffle, noiseMatrix)
nMask = -noiseMatrix

#for row masking to use little later
nMaskRow = copy(nMask)

#noise column mask[for loop manipulates individual columns with jumps per iteration]
namelessParam = pixInGrating*graRatioNum/graRatioDenum
for i in range(0,namelessParam):
    nMask[:,i::pixInGrating] = -1

#noise row mask
for i in range(0,namelessParam):
    nMaskRow[i::pixInGrating] = -1

blackPatch1 = visual.GratingStim(win=mywin, tex=None, mask=nMask, units='deg',
                                size=4,
                                color=lumColor,
                                interpolate=False,
                                )

blackPatch2 = visual.GratingStim(win=mywin, tex=None, mask=nMaskRow, units='deg',
                                size=4,
                                color=lumColor,
                                interpolate=False,
                                )

#%%
"""SET_POSITION_PREPERATION"""

pos1 = ( 0, 0)
pos2 = (-4, 0)
pos3 = ( 0, 0)
pos4 = ( 0,-4) 

posStep = 0.01

targetSwitch1 = -1 #for wednesday demo
targetSwitch2 = -1 #for wednesday demo


while True: #never-ending loop

    pos1 = ((pos1[0]+posStep)%4  ,pos1[1])
    pos2 = ((pos2[0]+posStep)%-4 ,pos2[1])
    pos3 = (pos3[0], (pos3[1]+posStep)%4 )
    pos4 = (pos4[0], (pos4[1]+posStep)%-4)

    
    #everything is done by clever positioning and drawing order
#    redPatch1.setPos(pos1)
#    redPatch1.draw()
#    redPatch1.setPos(pos2)
#    redPatch1.draw()

    redPatch1o.setPos(pos1)
    redPatch1o.draw()
    redPatch1o.setPos(pos2)
    redPatch1o.draw()

    if targetSwitch1 == 1: #demonstration target switch on off
        blackPatch1.setPos(pos1)
        blackPatch1.draw()
        blackPatch1.setPos(pos2)    
        blackPatch1.draw()

#    redPatch2.setPos(pos3)
#    redPatch2.draw()
#    redPatch2.setPos(pos4)
#    redPatch2.draw()

    redPatch2o.setPos(pos3)
    redPatch2o.draw()
    redPatch2o.setPos(pos4)
    redPatch2o.draw()

    if targetSwitch2 == 1: #demonstration target switch on off
        blackPatch2.setPos(pos3)
        blackPatch2.draw()
        blackPatch2.setPos(pos4)    
        blackPatch2.draw()


    mywin.flip()
    
    #handle key presses each frame
    for keys in event.getKeys(timeStamped=True):
        if keys[0]in ['escape','q']:
            mywin.close()
            core.quit()
            
        elif keys[0]in ['1']:
            grat_1 = grat_1+0.02
            grat_2 = grat_2-0.02
            redPatch1.setColor(grat_1)
            redPatch2.setColor(grat_2)
            redPatch1o.setColor(grat_1+colorDevience)
            redPatch2o.setColor(grat_2-colorDevience)

        elif keys[0]in ['2']:
            grat_1 = grat_1-0.02
            grat_2 = grat_2+0.02
            redPatch1.setColor(grat_1)
            redPatch2.setColor(grat_2)
            redPatch1o.setColor(grat_1-colorDevience)
            redPatch2o.setColor(grat_2+colorDevience)

        elif keys[0]in ['3']:
            colorDevience = colorDevience + 0.01
            redPatch1o.setColor(grat_1+colorDevience)
            redPatch2o.setColor(grat_2-colorDevience)

        elif keys[0]in ['4']:
            colorDevience = colorDevience - 0.01
            redPatch1o.setColor(grat_1+colorDevience)
            redPatch2o.setColor(grat_2-colorDevience)

        elif keys[0]in ['e']: #color reset
            grat_1 = 0
            grat_2 = 0
            redPatch1.setColor(grat_1)
            redPatch2.setColor(grat_2)
            redPatch1o.setColor(grat_1+colorDevience)
            redPatch2o.setColor(grat_2-colorDevience) 
           
        elif keys[0]in ['r']: #color devience reset
            colorDevience = 0
            redPatch1o.setColor(grat_1-colorDevience)
            redPatch2o.setColor(grat_2+colorDevience)
            
        elif keys[0]in ['t']:
            targetSwitch1 = targetSwitch1*(-1)

        elif keys[0]in ['y']:
            targetSwitch2 = targetSwitch2*(-1)

        elif keys[0]in ['o']:
            posStep = posStep*(-1)
  
        elif keys[0 ]in ['6']: #speed
            posStep = posStep + 0.001  

        elif keys[0]in ['5']: #speed
            posStep = posStep - 0.001
      
        elif keys[0]in ['0']: #speed
            posStep = 0

        elif keys[0]in ['9']: #speed
            posStep = 0.01
      