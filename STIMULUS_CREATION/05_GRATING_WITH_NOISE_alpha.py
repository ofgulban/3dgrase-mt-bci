# -*- coding: utf-8 -*-
"""
Created on Thu Feb 06 20:12:21 2014

Hit 'q' to quit.

@author: Research
"""

from psychopy import core, visual, event, misc, monitors
import numpy
import math
from copy import deepcopy

#%% Monitor

# set monitor information used in the experimental setup
moni = monitors.Monitor('UMRAM', width=8.2, distance=60) #cm,

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )

#%% black square

backSqr = visual.GratingStim(mywin, tex='none', units='deg', size=2.14, color=-1)



#%%

res = 16

#create a grid of xy vals
xys = misc.createXYs( numpy.linspace(-1,1,res) )# last controls resolution

#create opacity for each square in mask
opacs = numpy.ones(len(xys))#all opaque to start

#create mask
elSize = xys[1,0]-xys[0,0]
mask = visual.ElementArrayStim(mywin, elementTex=None, elementMask=None,
                               nElements = len(xys), units='deg',
                               colors  = 1, #smake all white so reducing opacity will make them black
                               xys   = xys, 
                               opacities=opacs, #all one
                               sizes = elSize,
                               )

maskIndices=numpy.arange(len(xys))
numpy.random.shuffle(maskIndices)

opacs[1::4] = 0 #make desired number of elements transparent so black
numpy.random.shuffle(opacs)


opacs2 = deepcopy(opacs) #deepcopy is required



opacs2 = numpy.reshape(opacs2, (16,16))

# square 1
for k in range (0,4):
    for i in range (0, 2):
        for j in range (0,4):    
            opacs2[(i*8)+j+4][k:16:8] = 0.2

# square 2
for k in range (0,4):
    for i in range (0, 2):
        for j in range (0,4):    
            opacs2[(i*8)+j+4][k+4:16:8] = 0.4

opacs2 = numpy.reshape(opacs2, (16*16))
opacs2 = opacs2 * opacs # array element multiplication for setting noise element opacities 0
opacs2 = numpy.reshape(opacs2, (16,16))



# square 3
for k in range (0,4):
    for i in range (0, 2):
        for j in range (0,4):    
            opacs2[(i*8)+j][k:16:8] = 0.6


# square 4
for k in range (0,4):
    for i in range (0, 2):
        for j in range (0,4):    
            opacs2[(i*8)+j][k+4:16:8] = 0


opacs2 = numpy.reshape(opacs2, (16*16))


#%%

# draw the stimuli and update the window
while True: #never-ending loop

    mask.setOpacities(opacs2)
    backSqr.draw()
    mask.draw()

    opacs2 = numpy.reshape(opacs2, (16,16))
    opacs2 = numpy.roll(opacs2,1,1)
    opacs2 = numpy.reshape(opacs2, (16*16))

    #handle key presses each frame
    for keys in event.getKeys(timeStamped=True):
        if keys[0]in ['escape','q']:
            mywin.close()
            core.quit()
    
    mywin.flip()

# cleanup
mywin.close()
core.quit()