# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 21:39:11 2014
Psychopy version: 1.79.01

Hit Q or Esc to quit

@author: Omer Faruk Gulban
"""

from psychopy import visual, core, event, filters, monitors, gui, misc 

import numpy as np
"""
If you a getting avbin.dll related memory error, you should use dedicated 
python interpreter instead of using 'Execute in current Python IPython 
interpreter
"""
#%% GUI

#load last run from config file
try:
    #try to load previous info
    config = misc.fromFile('config.pickle')
    print 'config.pickle is picked.'

#if loading fails use these values
except:
    #if no file use some defaults
    config = {'stimSize':4,
              'stimOpa' :0.5,              
              'stimFreq':1,
              'stimOri' :-15,
              'stimOriDiff':0,
              'stimPhaseStep':0.01,
              }
    print 'config.pickle is not picked! Using default configuration values.'

#create a user interface (DlgFromDict GUI)
configDlg = gui.DlgFromDict(dictionary = config, 
                            title = 'Please Check the Values:', 

                            #presentation order of the options
                            order = ['stimSize', 'stimOpa', 'stimFreq',
                                     'stimOri','stimOriDiff','stimPhaseStep',
                                     ]
                            )

#%% Monitor

# set monitor information used in the experimental setup
moni = monitors.Monitor('UMRAM', width=8.2, distance=60) #cm,
"""
From 'Silent Vision Model SV-7021' documentation.
"""

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
"""
'add' doesnt seem to work regardless of using PyOpenGL3.0
"""

#%%

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     = 'grey', 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'red' , 
                            size    = 0.05  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          interpolate=True,
                          contrast  =0,
                          size      =config['stimSize'],
                          ) 
mask.setAutoDraw(True)

#%%

# create 2x2 grid for texture
texG1 = np.array([[ -0.05, -0.05, -0.05, -0.1],   #square 1 & 2
                  [ -0.05, -0.05 ,-0.05, -0.1],
                  [ -0.05, -0.05 ,-0.05, -0.1],
                  [ -0.05, -0.05 ,-0.05, -0.1],
                  ])

# create gratings
grating1 = visual.GratingStim(win=mywin, tex=texG1, mask='circle', 
                              interpolate=False,
                              units     ='deg',
                              size      =config['stimSize'],
                              sf        =config['stimFreq'],
                              ori       =15,
                              opacity   =config['stimOpa' ],
                              texRes    =128,
                              contrast  =1,
                              )

grating2 = visual.GratingStim(win=mywin, tex=texG1, mask='circle',
                              interpolate=False,                              
                              units     ='deg'  ,                              
                              size      =config['stimSize'],
                              sf        =config['stimFreq'],
                              ori       =config['stimOri' ] + config['stimOriDiff'], #'+' for orientation diference
                              opacity   =config['stimOpa' ],
                              texRes    =128,
                              contrast  =1,
                              )

#%%

#this will be True (user hit OK) or False (user hit cancel)
if configDlg.OK: 
    
    # draw the stimuli and update the window
    while True: #never-ending loop
    
        grating1.setPhase(config['stimPhaseStep'], '+')    
        grating2.setPhase(config['stimPhaseStep'], '-')
    
        grating1.draw()
        grating2.draw()
    
        #handle key presses each frame
        for keys in event.getKeys(timeStamped=True):
            if keys[0]in ['escape','q']:
                mywin.close()
                core.quit()
        
        mywin.flip()

#%%

#save configurations
misc.toFile('config.txt', config)
print 'This run is completed. Configurations saved.'

# cleanup
mywin.close()
core.quit()