# -*- coding: utf-8 -*-
"""
Created on Thu Feb 06 20:12:21 2014

Hit 'q' to quit.

@author: Research
"""

from psychopy import core, visual, event, misc, monitors, filters
import numpy
from copy import deepcopy

#%% Monitor

# set monitor information used in the experimental setup
moni = monitors.Monitor('UMRAM', width=8.2, distance=60) #cm,

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )

#%% black square

backSqr = visual.GratingStim(mywin, tex='none', units='deg', size=4, color=-1)

#%%

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= False,                              
                              mask      = 'raisedCos', 
                              color     = 'grey', 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'red' , 
                            size    = 0.05  ,
                            )
fixDot.setAutoDraw(True)

#%% mask

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          interpolate=False,
                          contrast  =0,
                          size      =4.025,
                          ) 
mask.setAutoDraw(True)


#%% grating with noise texture

res = 256

#create a grid of xy vals
xys = misc.createXYs( numpy.linspace(-2,2,res) )# last controls resolution

#create opacity for each square in mask
opacs = numpy.ones(len(xys))#all opaque to start

#create mask
elSize = xys[1,0]-xys[0,0]
mask = visual.ElementArrayStim(mywin, elementTex=None, elementMask=None,
                               nElements = len(xys), units='deg',
                               colors  = 1, #smake all white so reducing opacity will make them black
                               xys   = xys, 
                               opacities=opacs, #all one
                               sizes = elSize, #do not change!!! BLUE SCREEN !!!
                               )

maskIndices=numpy.arange(len(xys))
numpy.random.shuffle(maskIndices)

# creating noise
opacs[1::36] = 0.8 #make desired number of elements transparent so black
numpy.random.shuffle(opacs)


opacs2 = deepcopy(opacs) #deepcopy is required

opacs2 = numpy.reshape(opacs2, (res,res))

#%%

scale = res/4 #scale factor for the square patterns

# square 1
for k in range (0,scale/2):
    for i in range (0, res/scale):
        for j in range (0,scale/2): 
            
            opacs2[(i*scale/1)+j+scale/2][k:res:scale] = 0.3

# square 2
for k in range (0,scale/2):
    for i in range (0, res/scale):
        for j in range (0,scale/2):    
            opacs2[(i*scale)+j+scale/2][k+scale/2:res:scale] = 0.4

opacs2 = numpy.reshape(opacs2, (res*res))
opacs2 = opacs2 * opacs # array element multiplication for setting noise element opacities 0
opacs2 = numpy.reshape(opacs2, (res,res))



# square 3 sol alttan sayarak aslında square 1
for k in range (0,scale/2):
    for i in range (0, res/scale):
        for j in range (0,scale/2):    
            opacs2[(i*scale)+j][k:res:scale] = 0.2


# square 4
for k in range (0,scale/2):
    for i in range (0, res/scale):
        for j in range (0,scale/2):    
            opacs2[(i*scale)+j][k+scale/2:res:scale] = 0.1


opacs2 = numpy.reshape(opacs2, (res*res))


#%%

# draw the stimuli and update the window
while True: #never-ending loop

    mask.setOpacities(opacs2)
    backSqr.draw()
    mask.draw()

    opacs2 = numpy.reshape(opacs2, (res,res))
    opacs2 = numpy.roll(opacs2,1,0)
    opacs2 = numpy.roll(opacs2,1,1)    
    opacs2 = numpy.reshape(opacs2, (res*res))

    #handle key presses each frame
    for keys in event.getKeys(timeStamped=True):
        if keys[0]in ['escape','q']:
            mywin.close()
            core.quit()
    
    mywin.flip()

# cleanup
mywin.close()
core.quit()