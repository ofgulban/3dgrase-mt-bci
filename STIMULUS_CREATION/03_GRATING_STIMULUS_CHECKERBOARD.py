# -*- coding: utf-8 -*-
"""
Created on Thu Feb 2 11:30:08 2014

-Numpy version 1.6.2
-Psychopy version: 1.79.01

Hit Q or Esc to quit.

@author: Omer Faruk Gulban
"""

import numpy as np

from psychopy import visual, core, event, filters, monitors

"""
If you are getting avbin.dll related memory error, you should use dedicated 
python interpreter instead of using 'Execute in current Python IPython 
interpreterç
"""

# set monitor information used in the experimental setup
moni = monitors.Monitor('UMRAM', width=8.2, distance=60) #cm, TODO: Correct width and distance values
"""
According to 'Silent Vision Model SV-7021' documentation.
"""

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'add' , #'add' doesnt seem to work regardless of using PyOpenGL3.0
                      )

#%%

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     = 'grey', 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'red' , 
                            size    = 0.05  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(128, shape='raisedCosine')  #has -1 for transp, +1 for opaque

# make opaque to transparent
invMaskTexture = -maskTexture
mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          interpolate=True,
                          contrast  =0,
                          size      =4,
                          ) 
mask.setAutoDraw(True)

#%%

# create 2x2 grid for texture
texG1 = np.array([[  -0.8, 0.8],
                  [   0.5, 0.5]])

# create gratings
grating1 = visual.GratingStim(win=mywin, tex=texG1, mask='circle', 
                              interpolate=False,
                              units ='deg'  ,
                              size  = 4     ,
                              texRes=128    ,
                              sf    =1      ,
                              ori   =0      ,
                              contrast  =1  ,
                              opacity   =1,
                              )

#%%

# draw the stimuli and update the window
while True: #never-ending loop

    grating1.setPhase(0.01, '+')    
    grating1.draw()

    #handle key presses each frame
    for keys in event.getKeys(timeStamped=True):
        if keys[0]in ['escape','q']:
            mywin.close()
            core.quit()
    
    mywin.flip()

# cleanup
mywin.close()
core.quit()