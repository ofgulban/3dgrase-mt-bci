# -*- coding: utf-8 -*-
"""
Created on Thu Feb 2 11:30:08 2014

-Numpy version 1.6.2
-Psychopy version: 1.79.01

INSTRUCTIONS:

-Hit 1 or 2 to bring grating1 or grating2 up.
-Hit 3 to make intersections average of grating1 and grating2.
-Hit 4 repeatedly to increase the value of intersection with small steps.
-Hit 5 repeatedly to decrease the value of intersection with small steps.
-Hit q
-Hit w
-Hit a
-Hit s
-Hit z
-Hit x
-Hit i
-Hit o
-Hit p

-Hit Esc to quit.

@author: Omer Faruk Gulban
"""

import numpy as np

from psychopy import visual, core, event, filters, monitors, gui, misc 
"""
If you are getting avbin.dll related memory error, you should use dedicated 
python interpreter instead of using 'Execute in current Python IPython 
interpreter.
"""
#%% GUI

#load last run from config file
try:
    #try to load previous info
    config = misc.fromFile('config.pickle')
    print 'config.pickle is picked.'

#if loading fails use these values
except:
    #if no file use some defaults
    config = {'stimSize':4,
              'stimOpa' :1,              
              'stimFreq':1,
              'stimOri' :0,
              'stimPhaseStep':0.01,
              'grating1': 0.5,
              'grating2': 0.3,
              'intersection': 0.4,
              'obj_background': 0.2,
              }
    print 'config.pickle is not picked! Using default configuration values.'

#create a user interface (DlgFromDict GUI)
configDlg = gui.DlgFromDict(dictionary = config, 
                            title = 'Configurations:', 

                            #presentation order of the options
                            order = ['stimSize', 'stimOpa', 'stimFreq',
                                     'stimOri','stimPhaseStep', 'grating1',
                                     'grating2', 'intersection', 'obj_background'
                                     ]
                            )

#%% Monitor

# set monitor information used in the experimental setup
moni = monitors.Monitor('UMRAM', width=8.2, distance=60) #cm,
"""
According to 'Silent Vision Model SV-7021' documentation.
"""

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
"""
'add' doesnt seem to work regardless of using PyOpenGL3.0
"""

#%%

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     = 'grey', 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'black' , 
                            size    = 0.05  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          interpolate=True,
                          contrast  =0,
                          size      =config['stimSize'],
                          ) 
mask.setAutoDraw(True)

#%%

plaid1 = config['grating1']
plaid2 = config['grating2']
intsec = config['intersection']
backgr = config['obj_background']


# create 2x2 grid for texture
texG1 = np.array([[  intsec,  plaid2,  plaid2,  plaid2 ],
                  [  plaid1,  backgr,  backgr,  backgr ],
                  [  plaid1,  backgr,  backgr,  backgr ],
                  [  plaid1,  backgr,  backgr,  backgr ],
                  ])




# create gratings
gratingStim = visual.GratingStim(win=mywin, tex=texG1, mask='circle',
                                 units     ='deg',
                                 size      =config['stimSize'],
                                  sf        =config['stimFreq'],
                                  ori       =config['stimOri' ],
                                  opacity   =config['stimOpa' ],
                                  )

#%%

# guidelines

guideline1 = visual.Rect(win=mywin, width=4, height=0.01, 
                         fillColor='black',
                         lineColor=None,
                         interpolate=False,
                         )

guideline2 = visual.Rect(win=mywin, width=0.01, height=4, 
                         fillColor='black',
                         lineColor=None,
                         interpolate=False,
                         )

guideline1.setAutoDraw(True)
guideline2.setAutoDraw(True)


#%%

# config text

debugText = 'Grating_1: %(grating1).2f'\
           '\nGrating_2: %(grating2).2f'\
           '\nIntersect: %(intersection).2f'\
           '\nObj_Bckgr: %(obj_background).2f'% config

cText= visual.TextStim(win=mywin, text=debugText, height= 0.125, 
                       pos=(3.2,-2.6), font='DeJaVu Sans Mono')

cText.setAutoDraw(True)

#%%
phaseSign = '+'

#this will be True (user hit OK) or False (user hit cancel)
if configDlg.OK: 
    
    # draw the stimuli and update the window
    while True: #never-ending loop
        
        gratingStim.setPhase(config['stimPhaseStep'], phaseSign)
        gratingStim.draw()


        #handle key presses each frame
        for keys in event.getKeys(timeStamped=True):

#intersection
            if keys[0]in ['1']:
                intsec = plaid1
                texG1[0][0] = intsec
                gratingStim.setTex(texG1)
                

            elif keys[0]in ['2']:
                intsec = plaid2
                texG1[0][0] = intsec
                gratingStim.setTex(texG1)            

            elif keys[0]in ['3']:
                intsec = (plaid1+plaid2)/2
                texG1[0][0] = intsec
                gratingStim.setTex(texG1)

            elif keys[0]in ['4']:
                intsec = intsec+0.02
                texG1[0][0] = intsec
                gratingStim.setTex(texG1)

            elif keys[0]in ['5']:
                intsec = intsec-0.02
                texG1[0][0] = intsec
                gratingStim.setTex(texG1)

#background
            elif keys[0]in ['z']:
                backgr = backgr-0.02
                texG1[1][1:4] = backgr
                texG1[2][1:4] = backgr
                texG1[3][1:4] = backgr
                gratingStim.setTex(texG1)


            elif keys[0]in ['x']:
                backgr = backgr+0.02
                texG1[1][1:4] = backgr
                texG1[2][1:4] = backgr
                texG1[3][1:4] = backgr
                gratingStim.setTex(texG1)

#plaid1
            elif keys[0]in ['q']:
                plaid1 = plaid1-0.02
                texG1[1][0] = plaid1
                texG1[2][0] = plaid1
                texG1[3][0] = plaid1
                gratingStim.setTex(texG1)

                
            elif keys[0]in ['w']:
                plaid1 = plaid1+0.02
                texG1[1][0] = plaid1
                texG1[2][0] = plaid1
                texG1[3][0] = plaid1
                gratingStim.setTex(texG1)

#plaid2
            elif keys[0]in ['s']:
                plaid2 = plaid2-0.02
                texG1[0][1:4] = plaid2
                gratingStim.setTex(texG1)
                
            elif keys[0]in ['a']:
                plaid2 = plaid2+0.02
                texG1[0][1:4] = plaid2
                gratingStim.setTex(texG1)
#orientation                
            elif keys[0]in ['o']:
                config['stimOri'] = config['stimOri'] + 90
                gratingStim.setOri(config['stimOri'])

            elif keys[0]in ['i']:
                phaseSign = '-'            

            elif keys[0]in ['p']:
                phaseSign = '+'
            
            elif keys[0]in ['escape']:
                mywin.close()
                #save configurations
                misc.toFile('config.pickle', config)
                print 'This run is completed. Configurations saved.'
                core.quit()            
            
            #variables to config
            config['grating1'] = plaid1
            config['grating2'] = plaid2
            config['intersection'] = intsec
            config['obj_background'] = backgr            

            # feels like not a proper way to update string            
            debugText = 'Grating_1: %(grating1).2f'\
           '\nGrating_2: %(grating2).2f'\
           '\nIntersect: %(intersection).2f'\
           '\nObj_Bckgr: %(obj_background).2f'% config 

            cText.setText(debugText)           
            
        mywin.flip()

#%%

# cleanup
mywin.close()
core.quit()