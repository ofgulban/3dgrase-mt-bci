# -*- coding: utf-8 -*-
"""
Created on Thu Feb 2 11:30:08 2014

-Numpy version 1.6.2
-Psychopy version: 1.79.01

Hit Q or Esc to quit.

@author: Omer Faruk Gulban
"""

import numpy as np

from psychopy import visual, core, event, filters, monitors, gui, misc 
"""
If you are getting avbin.dll related memory error, you should use dedicated 
python interpreter instead of using 'Execute in current Python IPython 
interpreter.
"""
#%% GUI

#load last run from config file
try:
    #try to load previous info
    config = misc.fromFile('config.pickle')
    print 'config.pickle is picked.'

#if loading fails use these values
except:
    #if no file use some defaults
    config = {'stimSize':4,
              'stimOpa' :1,              
              'stimFreq':1,
              'stimOri' :0,
              'stimPhaseStep':0.01,
              'square_1':-0.5,
              'square_2': 0.5,
              'square_3': 0.2,
              'square_4': 0.2,
              }
    print 'config.pickle is not picked! Using default configuration values.'

#create a user interface (DlgFromDict GUI)
configDlg = gui.DlgFromDict(dictionary = config, 
                            title = 'Configurations:', 

                            #presentation order of the options
                            order = ['stimSize', 'stimOpa', 'stimFreq',
                                     'stimOri','stimPhaseStep', 'square_1',
                                     'square_2', 'square_3', 'square_4'
                                     ]
                            )

#%% Monitor

# set monitor information used in the experimental setup
moni = monitors.Monitor('UMRAM', width=8.2, distance=60) #cm,
"""
According to 'Silent Vision Model SV-7021' documentation.
"""

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
"""
'add' doesnt seem to work regardless of using PyOpenGL3.0
"""

#%%

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     = 'grey', 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'red' , 
                            size    = 0.05  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          interpolate=True,
                          contrast  =0,
                          size      =config['stimSize'],
                          ) 
mask.setAutoDraw(True)

#%%

S=[config['square_1'], config['square_2'],
   config['square_3'], config['square_4']
   ]


# create 2x2 grid for texture
texG1 = np.array([[ S[0], S[1] ],   #square 1 & 2
                  [ S[2], S[3] ]    #square 3 & 4
                  ])

# create gratings
grating1 = visual.GratingStim(win=mywin, tex=texG1, mask='circle',
                              units     ='deg',
                              size      =config['stimSize'],
                              sf        =config['stimFreq'],
                              ori       =config['stimOri' ],
                              opacity   =config['stimOpa' ],
                              )

#%%

#this will be True (user hit OK) or False (user hit cancel)
if configDlg.OK: 
    
    # draw the stimuli and update the window
    while True: #never-ending loop
    
        grating1.setPhase(config['stimPhaseStep'], '+')
        grating1.draw()

        #handle key presses each frame
        for keys in event.getKeys(timeStamped=True):
            if keys[0]in ['escape','q']:
                mywin.close()
                core.quit()
        
        mywin.flip()

#%%

#save configurations
misc.toFile('config.txt', config)
print 'This run is completed. Configurations saved.'

# cleanup
mywin.close()
core.quit()