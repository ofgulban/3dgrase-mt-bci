// Batch script for MT Attention Project.
// MDM creation only
// @author: Omer Faruk Gulban 

var bvqx = BrainVoyagerQX;
bvqx.PrintToLog("---\n---");

//----------------------------------------------------------------
// Enter the following values:
var MainDataPath = "C:/Users/i6046143/Desktop/BV_SCRIPTING_TEST/";
var RunPrefix = "SC_RUN";
var NumberOfRuns = 2;
var AnatomicalData = MainDataPath + "ANATOMICAL/ANAT_ISO08.vmr";

var SDMPath = MainDataPath + "SDMs/";
var SDMList = ["SC_RUN_1.sdm",
               "SC_RUN_2.sdm"];
               
var MDMPrefix = "default"; //change this if for example shifted HRFs are used in .sdm's
//----------------------------------------------------------------

// Open an anatomical data
var docVMR = bvqx.OpenDocument(AnatomicalData);

// Clear any opened mdm's
docVMR.ClearMultiStudyGLMDefinition();

for(count = 1; count <= NumberOfRuns; count++){
    
    var RunName = RunPrefix + "_" + String(count);
    var RunPath = MainDataPath + RunName + "/";

    // Add .vtc's or .fmr's and .sdm's
    docVMR.AddStudyAndDesignMatrix(RunPath+RunName+".vtc", 
                                    SDMPath + SDMList[count-1]);
    
    bvqx.PrintToLog(RunName + " finished.");
};

docVMR.SaveMultiStudyGLMDefinitionFile(MainDataPath+"MDMs/"+RunPrefix+"_"+MDMPrefix+".mdm");

bvqx.PrintToLog("***\nThe_END\n***.");
