// Batch script for MT Attention Project.
// VTC creation only.
// @author: Omer Faruk Gulban 

var bvqx = BrainVoyagerQX;
bvqx.PrintToLog("---\n---");

//----------------------------------------------------------------
// Enter the following values:
var MainDataPath = "C:/Users/i6046143/Desktop/BV_SCRIPTING_TEST/";
var RunPrefix = "SC_RUN";
var NumberOfRuns = 2;
var AnatomicalData = MainDataPath + "ANATOMICAL/ANAT_ISO08.vmr";

// A specific reference volume for motion correction for all runs makes it possible to use
// the reference volume's IA and FA files for all other runs while creationg VTCs. In other
// case, specify each runs' .trf files and see lines 32 & 33 
var TRFPath = MainDataPath + "TRFs/";
var IA_TRFList = ["SC_03_THPGLMF4c_3DMCTS-TO-GESA_PILOT2_ISO08SINC_IA.trf"];
var FA_TRFList = ["SC_03_THPGLMF4c_3DMCTS-TO-GESA_PILOT2_ISO08SINC_FA.trf"];
//----------------------------------------------------------------

// Open an anatomical data
var docVMR = bvqx.OpenDocument(AnatomicalData);

for(count = 1; count <= NumberOfRuns; count++){
    
    var RunName = RunPrefix + "_" + String(count);
    var RunPath = MainDataPath + RunName + "/";

    // Create VTC
    docVMR.CreateVTCInVMRSpace(RunPath+RunName+".fmr", 
                                TRFPath + IA_TRFList[0], //in case of different IA files per run
                                TRFPath + FA_TRFList[0], //change 0 to count-1
                                RunPath+RunName+".vtc",  //output name
                                2,  //datatype integer 2-byte fomat:1 or float format:2
                                1,  //target resolution 1x1x1:1 or 2x2x2:2 or 3x3x3:3
                                2,  //nearest neighbor:0 or trilinear:1 or sinc:2
                                100 //threshold(Default value:100)
                                );
                                
    bvqx.PrintToLog(RunName + " finished.");
};

bvqx.PrintToLog("***\nThe_END\n***.");
