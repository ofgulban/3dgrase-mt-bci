// Batch script for MT Attention Project.
// Preprocessing + SDM creation.
// @author: Omer Faruk Gulban 

var bvqx = BrainVoyagerQX;
bvqx.PrintToLog("---\n---");

//----------------------------------------------------------------
// Enter the following values:
var MainDataPath = "C:/Users/i6046143/Desktop/BV_SCRIPTING_TEST/";
var RunPrefix = "SC_RUN";
var NumberOfRuns = 2;
//TODO: if only one reference volume will be used
//var TargetVolumeFile = ".fmr" //comment out line 32
//----------------------------------------------------------------

for(count = 1; count <= NumberOfRuns; count++){
    
    var RunName = RunPrefix + "_" + String(count);
    var RunPath = MainDataPath + RunName + "/";

    // Open the run
    var docFMR = bvqx.OpenDocument(RunPath+RunName+".fmr");
    
    // High-pass filtering (note: BV first applies linear trend removal [LTR])
    docFMR.TemporalHighPassFilterGLMFourier(4);
    ResultFileName = docFMR.FileNameOfPreprocessdFMR;
    docFMR.Close();
    docFMR = bvqx.OpenDocument(ResultFileName);

    // Motion Corection
//    var TargetVolumeFile = RunPath + RunName + "_firstvol.fmr"; //TODO: put this outside for loop
//    docFMR.CorrectMotionTargetVolumeInOtherRunEx(TargetVolumeFile,
//                                                 1, //target volume
//                                                 0, // 0 and 1:trilin./trilin., 2:trilin/sinc, 3:sinc/sinc
//                                                 false, //use full data set(default: false)
//                                                 100,   //maximum number of iterations(default: 100)
//                                                 false, //generate movie
//                                                 true   //motion estimation parameters in a text file
//                                                 );
//    ResultFileName = docFMR.FileNameOfPreprocessdFMR;
//    docFMR.Close();
//    docFMR = bvqx.OpenDocument(ResultFileName);

    // Temporal smoothing (if required);
//    docFMR.TemporalGaussianSmoothing(1, "TR");
//    ResultFileName = docFMR.FileNameOfPreprocessdFMR;
//    docFMR.Close();
//    docFMR = bvqx.OpenDocument(ResultFileName);

    // Design matrix(TODO: automatize across different types of runs)
    docFMR.ClearDesignMatrix();

    docFMR.AddPredictor("HORI_MO")
    docFMR.SetPredictorValuesFromCondition("HORI_MO", "HORI_MO", 1.0);
    docFMR.ApplyHemodynamicResponseFunctionToPredictor("HORI_MO");

    docFMR.AddPredictor("VERT_MO")
    docFMR.SetPredictorValuesFromCondition("VERT_MO", "VERT_MO", 1.0);
    docFMR.ApplyHemodynamicResponseFunctionToPredictor("VERT_MO");

    // Create SDM
    docFMR.SaveSingleStudyGLMDesignMatrix(MainDataPath + "SDMs/"+RunName+".sdm");
    
    // GLM
    docFMR.ComputeSingleStudyGLM();
    docFMR.ShowGLM();
    
    bvqx.PrintToLog(RunName + " finished.");
};

bvqx.PrintToLog("***\nThe_END\n***.");
