// Batch script for MT Attention Project.
// FMR creation + protocol linking.
// @author: Omer Faruk Gulban 

var bvqx = BrainVoyagerQX;
bvqx.PrintToLog("---\n---");

//----------------------------------------------------------------
// Enter the following values:
var MainDataPath = "C:/Users/i6046143/Desktop/BV_SCRIPTING_TEST/";
var RunPrefix = "SC_RUN";
var InDicomList = ["vkemp_140416_mtatt_pilot2 -0014-0001-00001.dcm",
                   "vkemp_140416_mtatt_pilot2 -0014-0001-00001.dcm"];

var InProtocolList = ["Gesa-SINGLE_COMPONENT_RUN-03.prt",
                      "Gesa-SINGLE_COMPONENT_RUN-04.prt"];

//----------------------------------------------------------------

var NrOfRuns = InDicomList.length;

for(count = 1; count <= NrOfRuns; count++){
    
    var RunName = RunPrefix + "_" + String(count);
    var RunPath = MainDataPath + RunName + "/";
    var InDicomName    = "DICOMS/" + InDicomList[count-1];
    var InProtocolName = "PROTOCOL/" + InProtocolList[count-1];

    // Create FMR Project (TODO: comment parameters)
    var docFMR = bvqx.CreateProjectMosaicFMR("DICOM", RunPath+InDicomName, 
                                             286,      //nr of volumes
                                             5,        //nr of volumes to skip
                                             true,     //create AMR
                                             10,       //nr of slices
                                             RunName,  //STC prefix
                                             false,    //swap bytes
                                             640, 120, //dimension of images in volume x, y
                                             2,        //nr of bytes per pixel, usually 2
                                             RunPath,  //saving directory
                                             1,        //number of volumes per file
                                             160, 30   //dimension of image x, y
                                             );

    // Link Protocol
    docFMR.LinkStimulationProtocol(RunPath + InProtocolName);

    // To give proper prefix names
    docFMR.SaveAs(RunName);

    bvqx.PrintToLog(RunName + " finished.");
};

bvqx.PrintToLog("***\nThe_END\n***.");
