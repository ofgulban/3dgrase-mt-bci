Please organize any session's main folder as follows:


-Put anatomical data and .vmr's to:

*/ANATOMICAL/

Organize functional data folders as:

*/RUN_1/DICOMS/
*/RUN_2/DICOMS/
.
.
.
*/RUN_n/DICOMS/

-Place functional data protocols as:

*/RUN_1/PROTOCOL/
*/RUN_2/PROTOCOL/
.
.
.
*/RUN_n/PROTOCOL/

-Must have folders in addition to data:

*/SDMs/
*/MDMs/
*/TRFs/

OR see Example_Foldering.png