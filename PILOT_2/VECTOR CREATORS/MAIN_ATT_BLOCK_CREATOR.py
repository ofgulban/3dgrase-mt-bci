# -*- coding: utf-8 -*-
"""
Created on Sun Apr 13 11:35:23 2014

@author: Research
"""

import numpy as np
#from random import randint

#%%
"""OPTIONS"""
smallRests = True
debug = False #odds: vert, evens: hori, zeros: rest

#%%
"""BLOCKS"""

# different numbers are blocks, same numbers indicate repetitions
staticBlocks  = np.array([[0, 1]]) 
staticBlocks2 = np.array([[2, 3]]) #swapped colored blocks

if debug:
    staticBlocks  = np.array([[0, 0]]) 
    staticBlocks2 = np.array([[0, 0]]) #swapped colored blocks

vertBlocks  =np.array([[ 4, 5, 6, 7,12,13,14,15]])
vertBlocks2 =np.array([[ 8, 9,10,11,16,17,18,19]]) #swapped colored blocks

horiBlocks  =np.array([[20,21,22,23,28,29,30,31]])
horiBlocks2 =np.array([[24,25,26,27,32,33,34,35]]) #swapped colored blocks

if debug:
    vertBlocks  =np.array([[ 1, 1, 1, 1, 1, 1, 1, 1]])
    vertBlocks2 =np.array([[ 1, 1, 1, 1, 1, 1, 1, 1]]) 

    horiBlocks  =np.array([[ 2, 2, 2, 2, 2, 2, 2, 2]]) 
    horiBlocks2 =np.array([[ 2, 2, 2, 2, 2, 2, 2, 2]]) 

def blockMultiplayer(vector, repetition):
    for i in range(0,repetition):
        vector = np.append(vector, vector, axis=1)
    print "Blocks_array:"; print vector; print "---"
    return vector

staticBlocks  = blockMultiplayer(staticBlocks, 2)
staticBlocks2 = blockMultiplayer(staticBlocks2, 2)

vertBlocks  = blockMultiplayer(vertBlocks, 1)
vertBlocks2 = blockMultiplayer(vertBlocks2, 1)

horiBlocks  = blockMultiplayer(horiBlocks, 1)
horiBlocks2 = blockMultiplayer(horiBlocks2, 1)


"""TARGET CONDITIONS"""
# append target conditions
vertTargetBlocks  = np.array([[99,99,98,98]])
horiTargetBlocks  = np.array([[97,97,96,96]])

if debug:
    vertTargetBlocks  = np.array([[1,1,1,1]])
    horiTargetBlocks  = np.array([[2,2,2,2]])

vertBlocks = np.append(vertBlocks, vertTargetBlocks, axis=1)
vertBlocks2 = np.append(vertBlocks2, vertTargetBlocks, axis=1)

horiBlocks = np.append(horiBlocks, horiTargetBlocks, axis=1)
horiBlocks2 = np.append(horiBlocks2, horiTargetBlocks, axis=1)


"""SHUFFLE"""
map(np.random.shuffle, staticBlocks)
print staticBlocks

map(np.random.shuffle, staticBlocks2)
print staticBlocks2

map(np.random.shuffle, vertBlocks)
print vertBlocks

map(np.random.shuffle, vertBlocks2)
print vertBlocks2

map(np.random.shuffle, horiBlocks)
print horiBlocks

map(np.random.shuffle, horiBlocks2)
print horiBlocks2


"""ONE VERT ONE HORI"""

blocks = np.array([[]])
blocks2 = np.array([[]])

for i in range(0,len(vertBlocks[0])):
    blocks = np.append(blocks, [[vertBlocks[0,i]]], axis=1)
    blocks = np.append(blocks, [[horiBlocks[0,i]]], axis=1)

    blocks2 = np.append(blocks2, [[vertBlocks2[0,i]]], axis=1)
    blocks2 = np.append(blocks2, [[horiBlocks2[0,i]]], axis=1)

"""SMALL_RESTS"""
# not one but two different insertions to change fixation dot color during rest.
if smallRests:
    for i in range(0,len(blocks[0])/2):
        blocks = np.insert(blocks,[(i*4)+0],101, axis=1)
        blocks = np.insert(blocks,[(i*4)+2],102, axis=1)

if smallRests:
    for i in range(0,len(blocks2[0])/2):
        blocks2 = np.insert(blocks2,[(i*4)+0],101, axis=1)
        blocks2 = np.insert(blocks2,[(i*4)+2],102, axis=1)

"""INSERT STATICS"""
# static distribution
staticDist  = np.array([[]]) #empty 2 dimentional array
staticDist2 = np.array([[]])
ar = 1

if smallRests:
    ar=2

# indices determination
for i in range(0,8):
    staticDist  = np.append(staticDist , [[(5*ar) + i*(5*ar) + i]], axis=1) #quite situational numbers
    staticDist2 = np.append(staticDist2, [[(5*ar) + i*(5*ar) + i]], axis=1)

# insertion    
for i, j in enumerate(staticDist[0]):
    blocks = np.insert(blocks, [j], staticBlocks[0,i], axis=1)

for i, j in enumerate(staticDist2[0]):
    blocks2 = np.insert(blocks2, [j], staticBlocks2[0,i], axis=1)

#%%
"""OFFSET"""

blocks = np.insert(blocks, 0, -1, axis=1)     #insert offset condition
#blocks = np.append(blocks, [[-1]], axis=1)    #insert final offset condition

blocks2 = np.insert(blocks2, 0, -1, axis=1)
#blocks2 = np.append(blocks2, [[-1]], axis=1)

print "Main Att Final blocks array:"; print repr(blocks); print "---"
print "Main Att Final blocks2 array for swapped colored runs:"; print repr(blocks2); print "---"