# -*- coding: utf-8 -*-
"""
Created on Sun Apr 13 11:35:23 2014

@author: Research
"""

import numpy as np
#from random import randint

#%%
"""OPTIONS"""
smallRests = True
debug = False #odds: vert, evens: hori, zeros: rest

#%%
"""BLOCKS"""

baseBlockDur = 6 #measurements (not in seconds!)

# different numbers are blocks, same numbers indicate repetitions
staticBlocks  = np.array([[0, 1]]) 
staticBlocks2 = np.array([[2, 3]]) #swapped colored blocks

vertBlocks  =np.array([[ 4, 5, 6, 7]])
vertBlocks2 =np.array([[ 8, 9,10,11]]) #swapped colored blocks

horiBlocks  =np.array([[12,13,14,15]])
horiBlocks2 =np.array([[16,17,18,19]]) #swapped colored blocks


def blockMultiplayer(vector):
    # *4 blocks
    vector = np.append(vector, vector, axis=1)
    vector = np.append(vector, vector, axis=1)
    print "Blocks_array:"; print vector; print "---"
    return vector

staticBlocks  = blockMultiplayer(staticBlocks)
staticBlocks2 = blockMultiplayer(staticBlocks2)

vertBlocks  = blockMultiplayer(vertBlocks)
vertBlocks2 = blockMultiplayer(vertBlocks2)

horiBlocks  = blockMultiplayer(horiBlocks)
horiBlocks2 = blockMultiplayer(horiBlocks2)

"""SHUFFLE"""
map(np.random.shuffle, staticBlocks)
print staticBlocks

map(np.random.shuffle, staticBlocks2)
print staticBlocks2

map(np.random.shuffle, vertBlocks)
print vertBlocks

map(np.random.shuffle, vertBlocks2)
print vertBlocks2

map(np.random.shuffle, horiBlocks)
print horiBlocks

map(np.random.shuffle, horiBlocks2)
print horiBlocks2


"""ONE VERT ONE HORI"""

# creation of run block vector
blocks = np.array([[]])
blocks2 = np.array([[]])

for i in range(0,len(vertBlocks[0])):
    blocks = np.append(blocks, [[vertBlocks[0,i]]], axis=1)
    blocks = np.append(blocks, [[horiBlocks[0,i]]], axis=1)

    blocks2 = np.append(blocks2, [[vertBlocks2[0,i]]], axis=1)
    blocks2 = np.append(blocks2, [[horiBlocks2[0,i]]], axis=1)

"""INSERT STATICS"""
# static distribution
staticDist  = np.array([[]]) #empty 2 dimentional array
staticDist2 = np.array([[]])
ar = 1

if smallRests:
    ar=2

# indices determination
for i in range(0,8):
    if i%2 == 0:
        staticDist  = np.append(staticDist , [[(2*ar) + i*(2*ar) + i+1]], axis=1) #quite situational numbers
        staticDist2 = np.append(staticDist2, [[(2*ar) + i*(2*ar) + i+1]], axis=1)

    if i%2 == 1:
        staticDist  = np.append(staticDist , [[(2*ar) + i*(2*ar) + i]], axis=1) #quite situational numbers
        staticDist2 = np.append(staticDist2, [[(2*ar) + i*(2*ar) + i]], axis=1)

# insertion    
for i, j in enumerate(staticDist[0]):
    blocks = np.insert(blocks, [j], staticBlocks[0,i], axis=1)

for i, j in enumerate(staticDist2[0]):
    blocks2 = np.insert(blocks2, [j], staticBlocks2[0,i], axis=1)

"""SMALL_RESTS"""
# not one but two different insertions to change fixation dot color during rest.
if smallRests:
    for i in range(0,len(blocks[0])):
        blocks = np.insert(blocks,[(i*2)+0],101, axis=1)

if smallRests:
    for i in range(0,len(blocks2[0])):
        blocks2 = np.insert(blocks2,[(i*2)+0],101, axis=1)
#%%
"""OFFSET"""

blocks = np.insert(blocks, 0, -1, axis=1)     #insert offset condition
#blocks = np.append(blocks, [[-1]], axis=1)    #insert final offset condition

blocks2 = np.insert(blocks2, 0, -1, axis=1)
#blocks2 = np.append(blocks2, [[-1]], axis=1)

print "Single Comp Final blocks array:"; print repr(blocks); print "---"
print "Single Comp Final blocks2 array for swapped colored runs:"; print repr(blocks2); print "---"