# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 11:13:54 2014

Hit ESC to quit

Press '1' or '2' to change motion directions.
Press '3' or '4' for luminance adjustments.
Press '9' or '0' for speed adjustments.
Press 'r' to reset.
Press 't' or 'y' for targets (noise on piecewise gratings)
Press 'z' or 'x' for color swap

Note: For attention run demonstrations:
    Red fixation dot is to attend horizontal motion (Press '5').
    Green fixation dot is to attend vertical motion (Press '6').
    Press '7' for black fixation dot.

@author: Omer Faruk Gulban
"""

import numpy as np
from copy import copy
from psychopy import visual, core, event, filters, monitors

#%%
"""GENERAL_OPTIONS"""

gratC       = (0.2,-0.2)
speedValue = 0.055

stimSize = 12

#%%
# set monitor information used in the experimental setup
moni = monitors.Monitor('testMonitor')
moni.setWidth(32)    #Pos1:32, Pos2:35, Nova:30 [cm]
moni.setDistance(70) #Pos1:70, Pos2:29.5, Nova:99 [cm]

print moni.getDistance()
print moni.getWidth()

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = True ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )

#%%
"""FIXATION"""

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     =-0.2, 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'black' , 
                            size    = 0.05  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

#%%
"""FIXATION"""

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     =-0.2, 
                              size      = 1, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'black' , 
                            size    = 0.13  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

#%%
"""RAISEDCOS_MASK"""

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          contrast=0, size=stimSize)
mask.setAutoDraw(True)

#%%
"""PERIPHERAL_MASK"""

#easy to use switch
peripheryMaskPowerSwitch=True

#TODO:Working but bad solution for peripheral masking
backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(stimSize,15), color='gray')
backgroundMask.setPos((-stimSize,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(stimSize,15), color='gray')
backgroundMask.setPos((+stimSize,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(15,stimSize), color='gray')
backgroundMask.setPos((0,-stimSize))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(15,stimSize), color='gray')
backgroundMask.setPos((0,+stimSize))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

#%%
"""PIECEWISE_GRATINGS"""

gratMask2 = -np.ones((16,16))
gratMask2[3::4,2::4] = 1
gratMask2[3::4,1::4] = 1

redPatch1o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg',
                                color=gratC[0],
                                size=stimSize,
                                sf=1,
                                ori=90,
                                interpolate=False,
                                )

redPatch2o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg', 
                                color=gratC[1],
                                size=stimSize,
                                sf=1,
                                ori=0,
                                interpolate=False,
                                )

#%%
"""NOISE_CREATION"""

#params
noiseColor = 0 #color of the noise

#noise related parameters
noiseReso = 128              #resolution of the noise
pixInGrating = noiseReso/4   #number of pixels within the width of a grating bar
noiseDensityFactor = 64 #higher values means less noise
graRatioNum = 3     #numerator (num&denum seperated due to type difference errors)
graRatioDenum = 4   #denumerator

#noise matrix operations
noiseMatrix = np.ones((noiseReso, noiseReso))
noiseMatrix[:,0::noiseDensityFactor] = -1

map(np.random.shuffle, noiseMatrix)
nMask = -noiseMatrix

#for row masking to use little later
nMaskRow = copy(nMask)

#noise column mask[for loop manipulates individual columns with jumps per iteration]
namelessParam = pixInGrating*graRatioNum/graRatioDenum
for i in range(0,namelessParam):
    nMask[:,i::pixInGrating] = -1

#cant think of an easy way now, just to delete non-overlaping noise (with piecewise gratings)
for i in range(0,namelessParam/3):
    nMask[i::pixInGrating] = -1
for i in range(0,namelessParam/3):
    nMask[i+24::pixInGrating] = -1

#noise row mask
for i in range(0,namelessParam/3):
    nMaskRow[i::pixInGrating] = -1
#ugly solution as above (TODO:think of a better way)
for i in range(0,namelessParam/3):
    nMaskRow[i+8::pixInGrating] = -1
for i in range(0,namelessParam/3):
    nMaskRow[i+16::pixInGrating] = -1    
for i in range(0,namelessParam/3):
    nMaskRow[:,i::pixInGrating] = -1
for i in range(0,namelessParam/3):
    nMaskRow[:,i+24::pixInGrating] = -1

blackPatch1 = visual.GratingStim(win=mywin, tex=None, mask=nMask, units='deg',
                                size=stimSize,
                                color=noiseColor,
                                interpolate=False,
                                )

blackPatch2 = visual.GratingStim(win=mywin, tex=None, mask=nMaskRow, units='deg',
                                size=stimSize,
                                color=noiseColor,
                                interpolate=False,
                                )

#%%
"""SET_POSITION_PREPERATION"""

pos1 = ( 0, 0)
pos2 = (-stimSize, 0)
pos3 = ( 0, 0)
pos4 = ( 0,-stimSize) 

posStep1 = speedValue
posStep2 = speedValue

targetSwitch1 = -1 #for wednesday demo
targetSwitch2 = -1 #for wednesday demo

layerSwitch = 1# used to change the drawing order


while True: #never-ending loop

    #comment out these 4 lines to use step by step mode
    pos1 = ((pos1[0]+posStep1)%stimSize  ,pos1[1])
    pos2 = ((pos2[0]+posStep1)%-stimSize ,pos2[1])
    pos3 = (pos3[0], (pos3[1]+posStep2)%stimSize )
    pos4 = (pos4[0], (pos4[1]+posStep2)%-stimSize)


    if layerSwitch ==1:
        redPatch1o.setPos(pos1)
        redPatch1o.draw()
        redPatch1o.setPos(pos2)
        redPatch1o.draw()
    
        if targetSwitch1 == 1: #demonstration target switch on off
            blackPatch1.setPos(pos1)
            blackPatch1.draw()
            blackPatch1.setPos(pos2)    
            blackPatch1.draw()

#------------------------------------------------------------------------------

#    redPatch2.setPos(pos3)
#    redPatch2.draw()
#    redPatch2.setPos(pos4)
#    redPatch2.draw()

    redPatch2o.setPos(pos3)
    redPatch2o.draw()
    redPatch2o.setPos(pos4)
    redPatch2o.draw()

    if targetSwitch2 == 1: #demonstration target switch on off
        blackPatch2.setPos(pos3)
        blackPatch2.draw()
        blackPatch2.setPos(pos4)    
        blackPatch2.draw()

#------------------------------------------------------------------------------

    elif layerSwitch ==-1:
        redPatch1o.setPos(pos1)
        redPatch1o.draw()
        redPatch1o.setPos(pos2)
        redPatch1o.draw()
    
        if targetSwitch1 == 1: #demonstration target switch on off
            blackPatch1.setPos(pos1)
            blackPatch1.draw()
            blackPatch1.setPos(pos2)    
            blackPatch1.draw()

#------------------------------------------------------------------------------

    mywin.flip()
    
    #handle key presses each frame
    for keys in event.getKeys(timeStamped=True):
        if keys[0]in ['escape','q']:
            print 'Grating_1: %.3f' %gratC[0]
            print 'Grating_2: %.3f' %gratC[1]
            mywin.close()
            core.quit()

        elif keys[0]in ['3']:
            gratC = [gratC[0], gratC[1] + 0.02]
            redPatch2o.setColor(gratC[1])
            gratC = [gratC[0] - 0.02, gratC[1]]
            redPatch1o.setColor(gratC[0])

        elif keys[0]in ['4']:
            gratC = [gratC[0], gratC[1] - 0.02]          
            redPatch2o.setColor(gratC[1])
            gratC = [gratC[0] + 0.02, gratC[1]]
            redPatch1o.setColor(gratC[0])
            
        elif keys[0]in ['t']:
            targetSwitch1 = targetSwitch1*(-1)

        elif keys[0]in ['y']:
            targetSwitch2 = targetSwitch2*(-1)

        elif keys[0]in ['1']:
            posStep1 = posStep1*(-1)

        elif keys[0]in ['2']:
            posStep2 = posStep2*(-1)

      
        elif keys[0]in ['0']: #speed
            posStep1 = 0
            posStep2 = 0  


        elif keys[0]in ['9']: #
            posStep1 = 0.01
            posStep2 = 0.01  

        elif keys[0]in ['z']: #color swap

            redPatch1o.setColor(gratC[1])
            redPatch2o.setColor(gratC[0])

        elif keys[0]in ['x']: #

            redPatch1o.setColor(gratC[0])
            redPatch2o.setColor(gratC[1])

        elif keys[0]in ['5']: #fixation dot
            fixDot.setColor('Red') #horizontal motion

        elif keys[0]in ['6']: # 
            fixDot.setColor('Green') #vertical motion

        elif keys[0]in ['7']: # 
            fixDot.setColor('Black')
      
        elif keys[0]in ['s']: #move one step
            pos1 = ((pos1[0]+posStep1)%stimSize  ,pos1[1])
            pos2 = ((pos2[0]+posStep1)%-stimSize ,pos2[1])
            pos3 = (pos3[0], (pos3[1]+posStep2)%stimSize )
            pos4 = (pos4[0], (pos4[1]+posStep2)%-stimSize)
            
        elif keys[0]in ['l']: #swap layers
            layerSwitch = layerSwitch*-1            
      
          