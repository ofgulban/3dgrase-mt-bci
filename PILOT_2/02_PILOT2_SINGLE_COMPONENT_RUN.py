# -*- coding: utf-8 -*-
"""
3D-GRASE MT project single component run script.

Uses Psychopy.
Hit ESC or q to quit.

Nr. of Measurements: 286

@author: Omer Faruk Gulban
"""

import numpy as np
from psychopy import visual, core, event, filters, monitors, parallel, misc
from os import mkdir
import time #to get date for the output file naming

#%%
"""GENERAL_OPTIONS"""
#******************************************************************************
#******************************************************************************
#******************************************************************************
subjectID = 'Gesa'
runNumber = '01'
offsetRandomNr1 = 0 #0 or 1 corresponding to runNumber[run01=0, run02=1, run03=0...] (fix for acroos run color swap bug in initial offset)

premadeVector = np.array([[  -1.,  101.,    5.,  101.,   15.,  101.,    7.,  101.,   15.,
         101.,    6.,  101.,    0.,  101.,   12.,  101.,    4.,  101.,
          12.,  101.,    0.,  101.,    7.,  101.,   14.,  101.,    4.,
         101.,   13.,  101.,    6.,  101.,    1.,  101.,   13.,  101.,
           4.,  101.,   14.,  101.,    0.,  101.,    7.,  101.,   12.,
         101.,    5.,  101.,   13.,  101.,    6.,  101.,    1.,  101.,
          15.,  101.,    5.,  101.,   13.,  101.,    1.,  101.,    6.,
         101.,   14.,  101.,    5.,  101.,   14.,  101.,    7.,  101.,
           0.,  101.,   15.,  101.,    4.,  101.,   12.,  101.,    1.]])

#******************************************************************************
#******************************************************************************
#******************************************************************************
          
fullScreen  = True
stimSize    = 12
gratC       = (0.7,-0.7) #grating colors (original demo value was 0.4)
speedValue = 0.055

smallRests = True

useParallelPort = False
parallelPin     = 12

triggerKey      ='5'

scriptID  = 'SINGLE_COMPONENT_RUN'
projectID = '3DGRASE_MT'

date = time.strftime("%b_%d_%Y_%H_%M", time.localtime())

#%%
"""MONITOR"""

# set monitor information used in the experimental setup
moni = monitors.Monitor('testMonitor') #cm,

moni.setWidth(32)    #Pos1:32, Pos2:35, Nova:30
moni.setDistance(70) #Pos1:70, Pos2:29.5, Nova:99

print moni.getDistance()
print moni.getWidth()

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = fullScreen ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      blendMode = 'avg' , 
                      )
mywin.setMouseVisible(False)
#%%
"""FIXATION"""

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     =-0.2, 
                              size      = 1, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'black', 
                            size    = 0.13, 
                            texRes  = 128,
                            )
fixDot.setAutoDraw(True)


#%%
"""RAISEDCOS_MASK"""

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          contrast=0, size=stimSize)
mask.setAutoDraw(True)

#%%
"""PERIPHERAL_MASK"""

#easy to use switch
peripheryMaskPowerSwitch=True

#TODO:Working but bad solution for peripheral masking
backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(stimSize,15), color='gray')
backgroundMask.setPos((-stimSize,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(stimSize,15), color='gray')
backgroundMask.setPos((+stimSize,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(15,stimSize), color='gray')
backgroundMask.setPos((0,-stimSize))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(15,stimSize), color='gray')
backgroundMask.setPos((0,+stimSize))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

#%%
"""TIME_TEST_TEXT"""
                              
testText = visual.TextStim(win=mywin, font="sans", height=0.3, pos=(0,-2.5), units='deg')
testText.setColor('Black')
testText.setAutoDraw(False)

#%%
"""PIECEWISE_GRATINGS"""

gratMask2 = -np.ones((16,16))
gratMask2[3::4,2::4] = 1
gratMask2[3::4,1::4] = 1

redPatch1o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg',
                                color=gratC[0],
                                size=stimSize,
                                sf=1,
                                ori=90,
                                interpolate=False,
                                )

redPatch2o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg', 
                                color=gratC[1],
                                size=stimSize,
                                sf=1,
                                ori=0,
                                interpolate=False,
                                )

#%%
"""SET_POSITION_PREPERATION"""

pos1 = ( 0, 0)
pos2 = (-stimSize, 0)
pos3 = ( 0, 0)
pos4 = ( 0,-stimSize) 

#%%
"""BLOCKS"""

baseBlockDur = 4 #measurements (not in seconds!)

# different numbers are blocks, same numbers indicate repetitions
blocks=premadeVector

#%%
"""JITTER"""

jitter=np.zeros((1,40))
jitter[0, 0:13]  = 1 #1 measurement as positive jitter (not in seconds!)
jitter[0, 13:27] = 2

# shuffle
map(np.random.shuffle, jitter)

#block durations after shuffling
blockDur = np.zeros((1,40))
blockDur = jitter + baseBlockDur

# small periodic rests
if smallRests:
    print 'Beware! Small rests are ON. \nLogical error will occur in case of wrong custom protocol array.\n---'
    for i in range(0,len(blockDur[0])):
        blockDur = np.insert(blockDur, [(i*2)+0], 2, axis=1) #3rd parameter is the duration


#%%
"""OFFSET"""

blockDur = np.insert(blockDur, 0,  5, axis=1)     #5 measurements of initial offset
#blockDur = np.append(blockDur, [[ 3]], axis=1)    #3 measurements final offset

print "Blocks_array:"; print blocks; print "---"
print "Block_duration_array:";print blockDur; print "---"

print 'HELLO'; print len(blocks[0]); print len(blockDur[0])

#%%
"""TIME"""

# params
fMRI_TR = 2
totalTime = np.sum(blockDur) * fMRI_TR

# just for cosmetic reasons 
totalTrigs = np.sum(blockDur)

print "Total_duration: %.3f min or %i sec" %(totalTime/60, totalTime)
print "Number of measurements: %i" %(np.sum(blockDur))
print "---"

# give the system time to settle
core.wait(0.5)

# create a clock
globalClock=core.Clock()
globalClock.reset()

#%%
"""SWITCHES"""

trigCounter = 0
layerSwitch = 1# used to change the drawing order
initialOffset = True

#%%
"""RENDER_LOOP"""

scannerStartTrigger = True
while scannerStartTrigger:

    if useParallelPort and parallel.readPin(parallelPin) == 1:
        scannerStartTrigger = False

    for keys in event.getKeys():        

        if keys in [triggerKey]:
            scannerStartTrigger = False

        elif keys[0]in ['escape','q']:
            mywin.close()
            core.quit()

i=0
interruption=False

while trigCounter<totalTrigs:

    #print which condition and index            
    print "Block_index___: %i [condition: %i]" %(i, blocks[0,i])

    if blocks[0,i] == -1: #offset
        posStepHori = 0
        posStepVert = 0
        
        if initialOffset:        
            #randomized layer swap
            layerSwitch = np.random.randint(2)*2-1
            #randomized color swap
            #offsetRandomNr1 = np.random.randint(2)
            redPatch1o.setColor(gratC[offsetRandomNr1])
            redPatch2o.setColor(gratC[offsetRandomNr1-1])
            #randomized offset variations
            offsetRandomNr2 = np.random.random()
            pos1 = (offsetRandomNr2, 0)
            pos2 = (offsetRandomNr2-stimSize, 0)
            pos3 = ( 0, offsetRandomNr2)
            pos4 = ( 0, offsetRandomNr2-stimSize) 
            initialOffset=False

    elif blocks[0,i] == 0: #static
        posStepHori=0
        posStepVert=0
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
 
    elif blocks[0,i] == 1: #static
        posStepHori=0
        posStepVert=0
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
       
    elif blocks[0,i] == 2: #static color swapped
        posStepHori=0
        posStepVert=0
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
    
    elif blocks[0,i] == 3: #static
        posStepHori=0
        posStepVert=0
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])


        
#Vertical_attention_conditions------------------------------------------------- 

    elif blocks[0,i] == 4: #vert
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])       
        posStepHori = 0
        posStepVert = speedValue
        
    elif blocks[0,i] == 5: #vert
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = 0
        posStepVert = speedValue

    elif blocks[0,i] == 6: #vert LAYER SWITCH  
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = 0
        posStepVert = -speedValue

    elif blocks[0,i] == 7: #vert
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = 0
        posStepVert = -speedValue

    elif blocks[0,i] == 8: #vert COLOR SWITCH
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = 0
        posStepVert = speedValue

    elif blocks[0,i] == 9: #vert
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = 0
        posStepVert = speedValue
        
    elif blocks[0,i] == 10: #vert LAYER SWITCH  
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = 0
        posStepVert = -speedValue

    elif blocks[0,i] == 11: #vert
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = 0
        posStepVert = -speedValue

    elif blocks[0,i] == 12: #hori CONDITION SWITCH, LAYER SWITCH
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = 0

    elif blocks[0,i] == 13: #hori
        layerSwitch   = -1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = 0

    elif blocks[0,i] == 14: #hori LAYER SWITCH
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = -speedValue
        posStepVert = 0

    elif blocks[0,i] == 15: #hori
        layerSwitch   = 1
        redPatch1o.setColor(gratC[0])
        redPatch2o.setColor(gratC[1])
        posStepHori = speedValue
        posStepVert = 0

    elif blocks[0,i] == 16: #hori COLOR SWITCH
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = 0

    elif blocks[0,i] == 17: #hori
        layerSwitch   = -1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = 0

    elif blocks[0,i] == 18: #hori LAYER SWITCH
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = -speedValue
        posStepVert = 0


    elif blocks[0,i] == 19: #hori
        layerSwitch   = 1
        redPatch1o.setColor(gratC[1])
        redPatch2o.setColor(gratC[0])
        posStepHori = speedValue
        posStepVert = 0

#Small_Rests-------------------------------------------------------------------

    elif blocks[0,i] == 101: #static
        posStepHori=0
        posStepVert=0
    
#------------------------------------------------------------------------------
    
    while trigCounter<np.sum(blockDur[0,0:i+1]):

        #set positions for motion
        pos1 = ((pos1[0]+posStepVert)%stimSize  ,pos1[1])
        pos2 = ((pos2[0]+posStepVert)%-stimSize ,pos2[1])
        pos3 = (pos3[0], (pos3[1]+posStepHori)%stimSize )
        pos4 = (pos4[0], (pos4[1]+posStepHori)%-stimSize)

        #----------------------------------------------------------------------

        if layerSwitch ==1:
            redPatch1o.setPos(pos1)
            redPatch1o.draw()
            redPatch1o.setPos(pos2)
            redPatch1o.draw()
                
        #----------------------------------------------------------------------
    
        redPatch2o.setPos(pos3)
        redPatch2o.draw()
        redPatch2o.setPos(pos4)
        redPatch2o.draw()
                
        #----------------------------------------------------------------------
    
        if layerSwitch ==-1:
            redPatch1o.setPos(pos1)
            redPatch1o.draw()
            redPatch1o.setPos(pos2)
            redPatch1o.draw()
         
        #----------------------------------------------------------------------

#        #globalClock on screen print (Beware: updating the text results FPS drops)
#        testText.setText("Block countdown = %.i" %(trigCounter-np.sum(blockDur[0,0:i+1])))
  
        mywin.flip()

        #parallel port trigger counter (see key press section below for usb triggers)
        if useParallelPort and parallel.readPin(parallelPin) == 1:
            trigCounter = trigCounter + 1

        #handle key presses each frame
        for keys in event.getKeys(timeStamped=True):
            if keys[0]in ['escape','q']:
                interruption = True

            if keys[0]in [triggerKey]:
                trigCounter = trigCounter + 1
        
    #break out nested loops
        if interruption:break
    if interruption:
        print '!!! \nInterrupted! \n!!! '
        break
        
    #print global time and condition duration to check time slips
    print "Global_Trigger_Count: %.i" %trigCounter
    print "Block_duration: %.i" %(trigCounter-np.sum(blockDur[0,0:i]))
    print "---"
        
    #iterate i
    i=i+1
    
# timing accuracy prints
finalTime = globalClock.getTime()
print "Finished in: %i seconds" %finalTime
print "***" 

#%%
"""OUTPUT"""

output = {'ProjectID'       : projectID,
          'ScriptID'        : scriptID,
          'Date'            : date,
          'SubjectID'       : subjectID,
          'Run_Number'      : runNumber,
          'Blocks'          : blocks,
          'Block_Durations' : blockDur,
          'Trigger_Count'   : trigCounter,
          'Final_Time'      : finalTime,
          'Interruption'    : interruption,
          'Stimulus_Size'   : stimSize,
          'Grating_Color'   : gratC,
          'Speed_Value'     : speedValue
          }
try:
    mkdir('OUTPUT', 0777)
except:
    print '(OUTPUT folder has already been created.)'
    
outFileName = subjectID +'-'+ scriptID +'-'+ runNumber +'-'+ date
misc.toFile('OUTPUT/'+ outFileName +'.pickle', output)
print 'Log Data saved as: '+ outFileName +'.pickle'
print "***"

#%% cleanup
mywin.close()
core.quit()