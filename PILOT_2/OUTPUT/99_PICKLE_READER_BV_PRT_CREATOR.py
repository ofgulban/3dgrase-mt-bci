# -*- coding: utf-8 -*-
"""
3D-GRASE MT project .pickle reader & custom .txt creator

Uses psychopy.misc because pickle.load function gives an error.
Will be used to create required custom .txt files by various software.

@author: Omer Faruk Gulban
"""

from psychopy import misc
from os import mkdir
from pprint import pprint

#%%
"""Please enter the exact name of the pickle file"""

inputFileName = 'Test-SINGLE_COMPONENT_RUN-04-Apr_15_2014_20_43.pickle'

smallRests = True

#%%
try:
    pickle = misc.fromFile(inputFileName)
except:
    print '!!!\nFile cannot be read!\n!!!'

#%% some easy to use notes

pprint(pickle)

print '---'

print pickle['Blocks']

print '---'

for i, j in enumerate(pickle['Blocks'][0]):
    if j in range(0,4) or j == 101:
        print pickle['Block_Durations'][0][i]

print '---'

#%% definitions
offsetIdx  = []
staticIdx  = []
horiIdx = []
vertIdx = []
targetVertIdx  = []
targetHoriIdx  = []

#%% file operations

filename = pickle['SubjectID'] +'-'+ pickle['ScriptID'] +'-'+ pickle['Run_Number'] +'-'+ pickle['Date'] + '.prt'
filePath = 'PROTOCOLS'

try:
    mkdir(filePath, 0777)
except:
    print '(OUTPUT folder has already been created.)'

file = open(filePath +'/'+ filename, 'w')

strings = ['FileVersion: 2\n',
           'ResolutionOfTime: Volumes\n',
           'Experiment: untitled\n',
           'BackgroundColor: 0 0 0\n',
           'TextColor: 255 255 217\n',
           'TimeCourseColor: 255 255 255\n',
           'TimeCourseThick: 3\n',
           'ReferenceFuncColor: 255 255 51\n',
           'ReferenceFuncThick: 2\n'
           ]

file.writelines(strings)

# correctly enter number of conditions according to the type of run
if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
    file.writelines('NrOfConditions: 4\n')
elif pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
    file.writelines('NrOfConditions: 5\n')


# find the indices of the conditions
for i, j in enumerate(pickle['Blocks'][0]):

    if j == -1:
        offsetIdx.append(i)

    if j in range(0,4) or j in range(101,103):
        staticIdx.append(i)

    # change ranges according to script ID (main run or single component run)
    if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
        if j in range(4,12):
            vertIdx.append(i)

    if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
        if j in range(4,20):
            vertIdx.append(i)

    if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
        if j in range(12,20):
            horiIdx.append(i)

    if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
        if j in range(20,36):
            horiIdx.append(i)

    if j in range(96,98):
        targetHoriIdx.append(i)

    if j in range(98,100):
        targetVertIdx.append(i)

#%%----------------------------------------------------------------------------
"""OFFSET"""

file.writelines(['\n','OFFSET\n','1'])

for i, j in enumerate(offsetIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 255 255 255')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""STATIC"""

file.writelines(['\n','STATIC\n','48'])

for i, j in enumerate(staticIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 200 200 200')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""ATTENTION_VERTICAL_MOTION"""

if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
    file.writelines(['\n','VERT_MO\n','16'])
elif pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
    file.writelines(['\n','ATTVERT\n','16'])

for i, j in enumerate(vertIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 255 0 0')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""ATTENTION_HORIZONTAL_MOTION"""

if pickle['ScriptID'] == 'SINGLE_COMPONENT_RUN':
    file.writelines(['\n','HORI_MO\n','16'])
elif pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
    file.writelines(['\n','ATTHORI\n','16'])

for i, j in enumerate(horiIdx):
    file.write('\n')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
    file.write(' ')
    file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

# contiditon color
file.write('\nColor: 0 255 0')
# empty space
file.write('\n')

#%%----------------------------------------------------------------------------
"""TARGET_DETECTION_VERT"""

if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
	file.writelines(['\n','TARGET_VERT\n','4'])

	for i, j in enumerate(targetVertIdx):
		file.write('\n')
		file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
		file.write(' ')
		file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

	# contiditon color
	file.write('\nColor: 255 30 60')
     # empty space
	file.write('\n')

"""TARGET_DETECTION_HORI"""

if pickle['ScriptID'] == 'MAIN_ATTENTION_RUN':
	file.writelines(['\n','TARGET_HORI\n','4'])

	for i, j in enumerate(targetHoriIdx):
		file.write('\n')
		file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])+1-pickle['Block_Durations'][0][j]  )  )  )
		file.write(' ')
		file.write(str(int(sum(pickle['Block_Durations'][0][0:j+1])  )  )  )

	# contiditon color
	file.write('\nColor: 30 60 255')
     # empty space
	file.write('\n')

file.close()