# -*- coding: utf-8 -*-
"""
Frame for randomized blocks and state changes of stimulus.

Hit ESC or q to quit.
"""

#!/usr/bin/env python
from psychopy import visual, monitors, core, event
import numpy as np

#%%
"""MONITOR"""

# set monitor information used in the experimental setup
moni = monitors.Monitor('testMonitor', width=8.2, distance=60) #cm,

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
                      
#%%
"""STIMULUS"""

testStim = visual.GratingStim(win=mywin, tex='sin', units='deg',
                              size=(3,3),
                              color='green',
                              )
                              
testText = visual.TextStim(win=mywin, font="sans", height=0.5)

#%%
"""BLOCKS"""

# different numbers are blocks, same numbers indicate repetitions
blocks=np.zeros((1,15)) #condition0
blocks[0,3:6]   = 1     #condition1
blocks[0,6:9]   = 2     #condition2
blocks[0,9:12]  = 3     #condition3
blocks[0,12:15] = 4     #condition4

# shuffle
map(np.random.shuffle, blocks)
print "Blocks_array:"; print blocks; print "---"

baseBlockDur = 2

#%%
"""JITTER"""

jitter=np.zeros((1,15))
jitter[0,3:6]   = 2
jitter[0,6:9]   = 2
jitter[0,9:12]  = 4
jitter[0,12:15] = 8

# shuffle
map(np.random.shuffle, jitter)

#block durations after shuffling
blockDur = np.zeros((1,15))
blockDur = jitter + baseBlockDur
print "Block_duration_array:";print blockDur; print "---"

#%%
"""TIME"""

# parameters
fMRI_TR = 2
totalTime = np.sum(blockDur)

print "Total_duration: %i seconds" %totalTime
print "Number of measurements(TR=%i): %i" %(fMRI_TR, totalTime/fMRI_TR)
print "---"

# give the system time to settle
core.wait(0.5)

# create a clock
globalClock=core.Clock()
blockClock=core.Clock()

globalClock.reset()
blockClock.reset()

#%%
"""RENDER_LOOP"""
i=0

while globalClock.getTime()<totalTime:
            

    if blocks[0,i] == 0:
        testStim.setColor('red')
        
    elif blocks[0,i] == 1:
        testStim.setColor('green')

    elif blocks[0,i] == 2:
        testStim.setColor('blue')

    elif blocks[0,i] == 3:
        testStim.setColor('pink')

    elif blocks[0,i] == 4:
        testStim.setColor('orange')

    while globalClock.getTime()<np.sum(blockDur[0,0:i+1]):

        #stimulus
        testStim.setPhase(0.01,'+')
        testStim.draw()

        #globalClock on screen print
        testText.setText("Block countdown = %.1f" %(globalClock.getTime()-np.sum(blockDur[0,0:i+1])))
        testText.setPos((0,-2))
        testText.setColor('white')
        testText.draw()
  
        mywin.flip()

        #handle key presses each frame
        for keys in event.getKeys(timeStamped=True):
            if keys[0]in ['escape','q']:
               mywin.close()
               core.quit()

    print "Block_index___: %i" %i
    print "Global_clock__: %.6f" %globalClock.getTime()
    print "Block_duration: %.6f" %(globalClock.getTime()-np.sum(blockDur[0,0:i]))
    print "---"
        
    #iterate i
    i=i+1

# timing accuracy prints
print "Finished in: %.6f seconds" %globalClock.getTime()
print "***"

# cleanup
mywin.close()
core.quit()