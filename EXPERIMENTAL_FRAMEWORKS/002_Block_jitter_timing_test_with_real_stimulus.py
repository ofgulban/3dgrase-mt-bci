# -*- coding: utf-8 -*-
"""
Frame for randomized blocks and state changes of stimulus with grating stimulus.

Hit ESC or q to quit.
"""

#!/usr/bin/env python
from psychopy import visual, monitors, core, event, filters
import numpy as np

#%%
"""MONITOR"""

# set monitor information used in the experimental setup
moni = monitors.Monitor('testMonitor', width=8.2, distance=60) #cm,

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
   
#%%
"""FIXATION"""

# fixation patch
fixPatch = visual.GratingStim(win=mywin, tex='none', units='deg', 
                              interpolate= True,                              
                              mask      = 'raisedCos', 
                              color     =-0.2, 
                              size      = 0.4, 
                              texRes    = 128,
                            )
fixPatch.setAutoDraw(True)

# fixation dot
fixDot = visual.GratingStim(win=mywin, tex='none', units='deg',
                            mask    = 'circle',
                            color   = 'black' , 
                            size    = 0.05  , 
                            texRes  = 128   ,
                            )
fixDot.setAutoDraw(True)

#%%
"""RAISEDCOS_MASK"""

# overlay an inverse raisedCosine mask (grey outside, transparent inside)
maskTexture = filters.makeMask(256, shape='raisedCosine')

# make opaque to transparent
invMaskTexture = -maskTexture

mask = visual.GratingStim(mywin, mask=invMaskTexture, tex=None, units='deg',
                          contrast=0, size=4)
mask.setAutoDraw(True)

#%%
"""PERIPHERAL_MASK"""

#easy to use switch
peripheryMaskPowerSwitch=True

#TODO:not the best peripheral masking
backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg', size=(4,7), color='gray')
backgroundMask.setPos((-4,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg',size=(4,7), color='gray')
backgroundMask.setPos((+4,0))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg',size=(7,4), color='gray')
backgroundMask.setPos((0,-4))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)

backgroundMask = visual.GratingStim(win=mywin, tex=None, units='deg',size=(7,4), color='gray')
backgroundMask.setPos((0,+4))
backgroundMask.setAutoDraw(peripheryMaskPowerSwitch)
                   
#%%
"""GRATINGS"""

#parameters
grat_1 = 0.3
grat_2 =-0.3
solid1 = 1
backgr =-1

# use same mask change orientation
gratMask = -np.ones((16,16))
gratMask[3::4] = 1 #stars from 3 to match with the stimulus

redPatch1 = visual.GratingStim(win=mywin, tex=None, mask=gratMask, units='deg',
                               color=grat_1,
                               size=4,
                               sf=1,
                               ori=90,
                               interpolate=False,
                               )

redPatch2 = visual.GratingStim(win=mywin, tex=None, mask=gratMask, units='deg', 
                               color=grat_2,
                               size=4,
                               sf=4,
                               ori=0,
                               interpolate=False,
                               )
#%%
"""COLOR_VARIATION_OVERLAYS"""

#params
colorDevience = 0.1

gratMask2 = -np.ones((16,16))
gratMask2[3::4,3::4] = 1
gratMask2[3::4,4::4] = 1

redPatch1o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg',
                                color=grat_1+colorDevience,
                                size=4,
                                sf=1,
                                ori=90,
                                interpolate=False,
                                )

redPatch2o = visual.GratingStim(win=mywin, tex=None, mask=gratMask2, units='deg', 
                                color=grat_2-colorDevience,
                                size=4,
                                sf=4,
                                ori=0,
                                interpolate=False,
                                )

#%%
"""SET_POSITION_PREPERATION"""

pos1 = ( 0, 0)
pos2 = (-4, 0)
pos3 = ( 0, 0)
pos4 = ( 0,-4) 

#%%
"""TIME_TEST_TEXT"""
                              
testText = visual.TextStim(win=mywin, font="sans", height=0.3, pos=(0,-2.5))
testText.setColor('Black')
testText.setAutoDraw(True)

#%%
"""BLOCKS"""

# different numbers are blocks, same numbers indicate repetitions
blocks=np.zeros((1,9)) #condition0
blocks[0,3:6]   = 1     #condition1
blocks[0,6:9]   = 2     #condition2
#blocks[0,9:12]  = 3     #condition3
#blocks[0,12:15] = 4     #condition4

# shuffle
map(np.random.shuffle, blocks)
print "Blocks_array:"; print blocks; print "---"

baseBlockDur = 2

#%%
"""JITTER"""

jitter=np.zeros((1,9))
jitter[0,3:6]   = 2
jitter[0,6:9]   = 4
#jitter[0,9:12]  = 4
#jitter[0,12:15] = 8

# shuffle
map(np.random.shuffle, jitter)

#block durations after shuffling
blockDur = np.zeros((1,9))
blockDur = jitter + baseBlockDur
print "Block_duration_array:";print blockDur; print "---"

#%%
"""TIME"""

# params
fMRI_TR = 2
totalTime = np.sum(blockDur)

print "Total_duration: %i seconds" %totalTime
print "Number of measurements(TR=%i): %i" %(fMRI_TR, totalTime/fMRI_TR)
print "---"

# give the system time to settle
core.wait(0.5)

# create a clock
globalClock=core.Clock()
blockClock=core.Clock()

globalClock.reset()
blockClock.reset()


#%%
"""RENDER_LOOP"""
i=0

while globalClock.getTime()<totalTime:

    #print which condition and index            
    print "Block_index___: %i [condition: %i]" %(i, blocks[0,i])

    if blocks[0,i] == 0: #stable
        posStep=0
        
    elif blocks[0,i] == 1: #up-down ori1
        posStep=0.01
    
    elif blocks[0,i] == 2: #down-up ori1
        posStep=-0.01

    while globalClock.getTime()<np.sum(blockDur[0,0:i+1]):

        pos1 = ((pos1[0]+posStep)%4  ,pos1[1])
        pos2 = ((pos2[0]+posStep)%-4,pos2[1])
        pos3 = (pos3[0], (pos3[1]+posStep)%4)
        pos4 = (pos4[0], (pos4[1]+posStep)%-4)

        #stimulus
        redPatch1.setPos(pos1)
        redPatch1.draw()
        redPatch1.setPos(pos2)
        redPatch1.draw()
    
        redPatch1o.setPos(pos1)
        redPatch1o.draw()
        redPatch1o.setPos(pos2)
        redPatch1o.draw()
    
        redPatch2.setPos(pos3)
        redPatch2.draw()
        redPatch2.setPos(pos4)
        redPatch2.draw()
    
        redPatch2o.setPos(pos3)
        redPatch2o.draw()
        redPatch2o.setPos(pos4)
        redPatch2o.draw()

        #globalClock on screen print (Beware: updating the text results FPS drops)
        testText.setText("Block countdown = %.1f" %(globalClock.getTime()-np.sum(blockDur[0,0:i+1])))
  
        mywin.flip()

        #handle key presses each frame
        for keys in event.getKeys(timeStamped=True):
            if keys[0]in ['escape','q']:
               mywin.close()
               core.quit()

    #print global time and condition duration to check time slips
    print "Global_clock__: %.6f" %globalClock.getTime()
    print "Block_duration: %.6f" %(globalClock.getTime()-np.sum(blockDur[0,0:i]))
    print "---"
        
    #iterate i
    i=i+1

# timing accuracy prints
print "Finished in: %.6f seconds" %globalClock.getTime()
print "***"

# cleanup
mywin.close()
core.quit()