# -*- coding: utf-8 -*-
"""
Minimal frame.
"""

#!/usr/bin/env python
from psychopy import visual, monitors, core, event

#%%
"""MONITOR"""

# set monitor information used in the experimental setup
moni = monitors.Monitor('testMonitor', width=8.2, distance=60) #cm,

# set screen (make 'fullscr = True' for fullscreen)
mywin = visual.Window(size =(1024, 768) , screen = 0, winType='pyglet', 
                      allowGUI  = True  , allowStencil = True,                      
                      fullscr   = False ,
                      monitor   = moni  ,
                      color     = 'grey',
                      colorSpace= 'rgb' ,
                      units     = 'cm'  ,
                      blendMode = 'avg' , 
                      )
                      
#%%
"""STIMULUS"""

testStim = visual.GratingStim(win=mywin, tex=None, units='deg', 
                              size=(3,3),
                              color='green',
                              )
                              
testText = visual.TextStim(win=mywin, color='red', height=0.5)

#%%
"""TIME"""

# parameters
totalTime = 5.

# give the system time to settle
core.wait(0.5)

# create a clock
clock=core.Clock()
clock.reset()

#%%
"""RENDER_LOOP"""

while clock.getTime()<totalTime:

    # set test text
    testText.setText(clock.getTime())
    
    testStim.draw()
    testText.draw()

    mywin.flip()
    
    #handle key presses each frame
    for keys in event.getKeys(timeStamped=True):
        if keys[0]in ['escape','q']:
           mywin.close()
           core.quit()     

mywin.close()
core.quit()