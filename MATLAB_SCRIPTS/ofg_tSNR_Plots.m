function [] = ofg_tSNR_Plots(dataPath, varargin)
%TSNR_LIGHTBOXPLOTS Temporal signal to noise ratio lightbox plots
%
%   This script requires NeuroElf.
%
%   ofg_tSNR_Plots('C:\Data\SC_02_THPGLMF4c_Residuals.fmr')
%   calculates tSNR and plots resulting images as 2 by 5 tiling by default.
%
%   ofg_tSNR_Plots('C:\Data\SC_02_THPGLMF4c_Residuals.fmr', 3, 6)
%   calculates tSNR and plots resulting images as 3 by 6 tiling.
%
%   ofg_tSNR_Plots('C:\Data\SC_02_THPGLMF4c_Residuals.fmr', 3, 6, 'off')
%   calculates tSNR and plots resulting images as 3 by 6 tiling without
%   color bar.
%
%   @author:Omer Faruk Gulban 25.4.2014

%% Optional inputs
%This section follows:
%"http://blogs.mathworks.com/loren/2009/05/05/nice-way-to-set-function-defaults/"

%Only 3 optional inputs
numvarargs = length(varargin);
if numvarargs > 3
    error('myfuns:somefun2Alt:TooManyInputs', ...
          'requires at most 3 optional inputs');
end

%defaults for optional inputs
optargs = {2 5 'on'};

% now put these defaults into the valuesToUse cell array, 
% and overwrite the ones specified in varargin.
optargs(1:numvarargs) = varargin;

% Place optional args in memorable variable names
[nrPlotRows, nrPlotColumns, commonColorBar] = optargs{:};

%% Load Data

waitbarHandle = waitbar(0, 'Initializing...');

timeSeries     = xff(dataPath);
stcTimeSeries  = double(timeSeries.Slice.STCData);
sizeTimeSeries = size(stcTimeSeries);

disp('Size:')
disp(sizeTimeSeries)

%% Trim extra rows
%x y should be 160 30 instead of 160 160
stcTimeSeries = stcTimeSeries(:, 66:95, :, :);

%check size
sizeTimeSeries = size(stcTimeSeries);
disp('After trimming:')
disp(sizeTimeSeries)

%% tSNR, mean/std

stc1_mean = mean(stcTimeSeries, 3);
stc1_std  = std(stcTimeSeries, 0, 3);
stc1_tSNR = rdivide(stc1_mean, stc1_std);

%% Plotting

fig = figure; %for saving later
for i=1:sizeTimeSeries(4) %number of slices

    waitbar(i/sizeTimeSeries(4),waitbarHandle, 'Progress...');
    
    %subplot(nrPlotColumns, nrPlotRows, i) % for default subplot behavior
    subtightplot(nrPlotColumns, nrPlotRows, i); %uses a third party script
    imagesc(stc1_tSNR(:,:,i)');
    daspect([1 1 1]);
    caxis([0 25]); %Pseudocolor axis scaling
    title(['Slice', num2str(i)]);
    
end

waitbar(1,waitbarHandle, 'Finalizing...');

% all axes off
set(findobj(gcf, 'type','axes'), 'Visible','off');

%Color bar
if strcmp(commonColorBar, 'on') ; 
    cbar = colorbar;
    caxis([0 25]);
    set(cbar, 'Position', [0.97 0.05 0.005 0.80]);
end

%Common title
commonTitle = strrep(timeSeries.Prefix, '_', '\_'); %to fix underscores
supT = suptitle(['tSNR Images ', commonTitle]);
set(supT,'FontName','Arial', 'FontSize',20,'FontWeight','normal');

% %Figure window position
set(1, 'Position', [500 100 1200 800]);

%Save figure
%saveas(fig, timeSeries.Prefix, 'png');
saveas(fig, 'test', 'png');

close(waitbarHandle);