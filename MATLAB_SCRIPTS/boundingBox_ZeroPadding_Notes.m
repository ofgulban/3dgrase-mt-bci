% Brainvoyager VTC bounding box mismatch error zero padding solution.
%
% @author: Omer Faruk Gulban

runData = xff('TEST_LTR_THPGLMF4c_3DMCTS.vtc')

%Example output(before):
%
% FileVersion: 3
% NameOfSourceFMR: [1x72 char]
% NrOfLinkedPRTs: 1
% NameOfLinkedPRT: [1x84 char]
% NrOfCurrentPRT: 1
% DataType: 2
% NrOfVolumes: 281
% Resolution: 1
% XStart: 185
% XEnd: 216
% YStart: 78
% YEnd: 102
% ZStart: 52
% ZEnd: 211
% Convention: 1
% ReferenceSpace: 1
% HemodynamicDelay: 1
% TR: 2000
% HrfDelta: 2.5
% HrfTau: 1.25
% SegmentSize: 10
% SegmentOffset: 0
% VTCData: [281x31x24x159 single]
% RunTimeVars: [1x1 struct]

runData.XEnd = 217;
runData.VTCData = padarray(runData.VTCData, [0,1,0,0], 0, 'post'); %or 'pre'

runData

%Example output(after):
%
% FileVersion: 3
% NameOfSourceFMR: [1x72 char]
% NrOfLinkedPRTs: 1
% NameOfLinkedPRT: [1x84 char]
% NrOfCurrentPRT: 1
% DataType: 2
% NrOfVolumes: 281
% Resolution: 1
% XStart: 185
% XEnd: 217    <<<
% YStart: 78
% YEnd: 102
% ZStart: 52
% ZEnd: 211
% Convention: 1
% ReferenceSpace: 1
% HemodynamicDelay: 1
% TR: 2000
% HrfDelta: 2.5
% HrfTau: 1.25
% SegmentSize: 10
% SegmentOffset: 0
% VTCData: [281x32x24x159 single]    <<<
% RunTimeVars: [1x1 struct]

%%
runData.save

%% Just as a note:
%% For trimming(cropping)
%runData.VTCData(:,:,24,:)=[];
