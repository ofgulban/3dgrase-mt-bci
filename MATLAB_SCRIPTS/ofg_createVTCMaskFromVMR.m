%% VMR whole brain - VTC conjunction mask creator
%
%@author: Omer Faruk Gulban (25/06/2014)

% Load
anat = xff('ANAT_ISO08.vmr');
vtc  = xff('SC_2_LTR_THPGLMF4c_3DMCTS.vtc');
mask = xff('SC_MOTIONONLY_BOTH.msk');

% Please specify the resolution
outResolution = 1;

%% All nonzeros to 1 in VMR
iMat = anat.VMRData;
iMat = iMat~=0;

%% VTC zero padding using VMR dimensions

vtc.VTCData = shiftdim(vtc.VTCData, 1);
vtc.VTCData(:,:,:, 2:end) = [];
vtcDims = size(vtc.VTCData); %for later use

%vtc upsample
if outResolution > 1;
    
    vtcDims = size(vtc.VTCData);
    Xq = zeros(vtcDims(1)*outResolution, vtcDims(2)*outResolution, vtcDims(3)*outResolution);

    vtc.VTCData = single(vtc.VTCData);
    vtc.VTCData = interp3(vtc.VTCData,Xq,Xq,Xq);
    vtc.VTCData = uint8(vtc.VTCData);
end

disp('before padding iVTC:')
disp(size(vtc.VTCData))

vtc.VTCData = padarray(vtc.VTCData, [vtc.XStart, 0, 0], 0, 'pre');
vtc.VTCData = padarray(vtc.VTCData, [0, vtc.YStart, 0], 0, 'pre');
vtc.VTCData = padarray(vtc.VTCData, [0, 0, vtc.ZStart], 0, 'pre');

vtc.VTCData = padarray(vtc.VTCData, [anat.DimX-vtc.XEnd, 0, 0], 0, 'post');
vtc.VTCData = padarray(vtc.VTCData, [0, anat.DimY-vtc.YEnd, 0], 0, 'post');
vtc.VTCData = padarray(vtc.VTCData, [0, 0, anat.DimZ-vtc.ZEnd], 0, 'post');

%% Conjunction vtc and mask then crop

disp('after padding iVTC:')
disp(size(vtc.VTCData))
disp('imat:')
disp(size(iMat))

vtc.VTCData = single(vtc.VTCData);
iMat = single(iMat);
sampVtc = vtc.VTCData;

iMat = vtc.VTCData.*iMat;
iMat = iMat~=0; %nonzeros to 1

disp('imat after multiplication:')
disp(size(iMat))

%crop
iMat(1:vtc.XStart, :, :) = [];
iMat(:, 1:vtc.YStart, :) = [];
iMat(:, :, 1:vtc.ZStart) = [];

disp('imat midcrop:')
disp(size(iMat))

iMat(vtc.XEnd-vtc.XStart+1:end, :, :) = [];
iMat(:, vtc.YEnd-vtc.YStart+1:end, :) = [];
iMat(:, :, vtc.ZEnd-vtc.ZStart+1:end) = [];

disp('imat after crop:')
disp(size(iMat))

%% Interpolate mask to VTC resolution

if outResolution > 1;
    Xq = zeros(vtcDims(1), vtcDims(2), vtcDims(3));

    iMat = single(iMat);
    iMat = interp3(iMat,Xq,Xq,Xq);
end

iMat = iMat~=0;
iMat = uint8(iMat);

disp('iMat before save size:')
disp(size(iMat))

%timer: 3 hours 40 minutes

%% Put new info to mask
mask.Mask = iMat;
mask.Resolution = outResolution;

mask.XStart = vtc.XStart;
mask.XEnd   = vtc.XEnd;
mask.YStart = vtc.YStart;
mask.YEnd   = vtc.YEnd;
mask.ZStart = vtc.ZStart;
mask.ZEnd   = vtc.ZEnd;

mask.save